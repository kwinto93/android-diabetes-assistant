package info.mkordula.java.database;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import java.util.Date;

@Entity
@SuppressWarnings("all")
public class LogEntryModel {
    public static final long UNKNOWN_ITEM_ID = -1L;

    @Id(autoincrement = true)
    private Long id;
    private Date date;
    private int glucose;
    private int primaryInsulinDose;
    private int secondaryInsulinDose;
    private int carbohydratesDose;
    private double weight;

    @Generated(hash = 1567561294)
    public LogEntryModel(Long id, Date date, int glucose, int primaryInsulinDose,
            int secondaryInsulinDose, int carbohydratesDose, double weight) {
        this.id = id;
        this.date = date;
        this.glucose = glucose;
        this.primaryInsulinDose = primaryInsulinDose;
        this.secondaryInsulinDose = secondaryInsulinDose;
        this.carbohydratesDose = carbohydratesDose;
        this.weight = weight;
    }
    @Generated(hash = 1211359327)
    public LogEntryModel() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    @SuppressWarnings("EI_EXPOSE_REP")
    public Date getDate() {
        return this.date;
    }
    @SuppressWarnings("EI_EXPOSE_REP2")
    public void setDate(Date date) {
        this.date = date;
    }
    public int getGlucose() {
        return this.glucose;
    }
    public void setGlucose(int glucose) {
        this.glucose = glucose;
    }
    public int getPrimaryInsulinDose() {
        return this.primaryInsulinDose;
    }
    public void setPrimaryInsulinDose(int primaryInsulinDose) {
        this.primaryInsulinDose = primaryInsulinDose;
    }
    public int getSecondaryInsulinDose() {
        return this.secondaryInsulinDose;
    }
    public void setSecondaryInsulinDose(int secondaryInsulinDose) {
        this.secondaryInsulinDose = secondaryInsulinDose;
    }
    public int getCarbohydratesDose() {
        return this.carbohydratesDose;
    }
    public void setCarbohydratesDose(int carbohydratesDose) {
        this.carbohydratesDose = carbohydratesDose;
    }
    public double getWeight() {
        return this.weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
}