package info.mkordula.core;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;


public abstract class BaseFragment extends Fragment {
    @Override
    public final View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = getViewLayout(inflater, container, savedInstanceState);
        ButterKnife.bind(this, view);
        onSetup(savedInstanceState);
        return view;
    }

    protected void onSetup(Bundle savedInstanceState) {
        // can be overridden
    }

    protected abstract View getViewLayout(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState);
}
