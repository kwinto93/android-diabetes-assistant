package info.mkordula.core;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        onSetup(savedInstanceState);
    }

    @LayoutRes
    protected abstract int getLayoutId();

    /**
     * actions performed after onCreate
     */
    protected abstract void onSetup(@Nullable Bundle savedInstanceState);
}
