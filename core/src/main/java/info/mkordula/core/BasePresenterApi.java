package info.mkordula.core;

public interface BasePresenterApi<V> {
    void attach(V view);
    void detach();
}
