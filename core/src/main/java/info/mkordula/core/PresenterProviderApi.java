package info.mkordula.core;

interface PresenterProviderApi<P> {
    void setPresenter(P presenter);
}
