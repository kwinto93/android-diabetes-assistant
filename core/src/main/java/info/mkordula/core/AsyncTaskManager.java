package info.mkordula.core;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class AsyncTaskManager {
    public static <R> void execute(@NonNull final BackgroundJob<R> job, @Nullable final Runnable pre,
                                   @Nullable final PostExecuteJob<R> post) {
        new SimpleAsyncTask<>(job, pre, post)
                .execute();
    }

    public interface BackgroundJob<R> {
        R doInBackground();
    }

    public interface PostExecuteJob<R> {
        void postExecute(R result);
    }

    private static class SimpleAsyncTask<R> extends AsyncTask<Void, Void, R> {
        @NonNull
        private final BackgroundJob<R> mJob;
        @Nullable
        private final Runnable mPre;
        @Nullable
        private final PostExecuteJob<R> mPost;

        SimpleAsyncTask(@NonNull BackgroundJob<R> job, @Nullable Runnable pre,
                        @Nullable PostExecuteJob<R> post) {
            mJob = job;
            mPre = pre;
            mPost = post;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            if (mPre != null) {
                mPre.run();
            }
        }

        @Override
        protected R doInBackground(Void... voids) {
            return mJob.doInBackground();
        }

        @Override
        protected void onPostExecute(R result) {
            if (mPost != null) {
                mPost.postExecute(result);
            }

            super.onPostExecute(result);
        }
    }
}
