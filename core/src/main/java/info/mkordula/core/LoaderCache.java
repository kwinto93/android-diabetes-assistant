package info.mkordula.core;

import android.content.Context;
import android.support.v4.content.Loader;
import android.util.Log;

public abstract class LoaderCache<D> extends Loader<D> {
    private static final String LOADER_CACHE_TAG = "LoaderCache";
    private D mCache;

    public LoaderCache(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        Log.i(LOADER_CACHE_TAG, "Start loading");

        if (mCache != null) {
            deliverResult(mCache);
        } else {
            mCache = createCachedObject();
        }
    }

    protected abstract D createCachedObject();

    @Override
    protected void onReset() {
        super.onReset();
        Log.i(LOADER_CACHE_TAG, "On reset");
        onStopLoading();
    }

    @Override
    protected void onStopLoading() {
        Log.i(LOADER_CACHE_TAG, "Stop loading");
        cancelLoad();
        super.onStopLoading();
    }

    @Override
    protected boolean onCancelLoad() {
        Log.i(LOADER_CACHE_TAG, "Cancel load");
        mCache = null;
        return super.onCancelLoad();
    }

    @Override
    public void deliverResult(D data) {
        Log.i(LOADER_CACHE_TAG, "Deliver result");
        if (isReset() || isAbandoned()) {
            cancelLoad();
        } else if (isStarted()) {
            super.deliverResult(data);
        }
    }
}
