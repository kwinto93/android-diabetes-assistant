@file:Suppress("UNCHECKED_CAST")

package info.mkordula.opendiabetesassistant.utils

import org.mockito.Mockito

object TestHelper {
    fun <T> anyKotlin(): T {
        Mockito.any<T>()
        return uninitialized()
    }

    private fun <T> uninitialized(): T = null as T
}