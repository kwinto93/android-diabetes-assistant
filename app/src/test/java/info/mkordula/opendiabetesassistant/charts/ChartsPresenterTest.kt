package info.mkordula.opendiabetesassistant.charts

import android.view.View
import info.mkordula.opendiabetesassistant.data.database.events.DatabaseCacheChangedEvent
import info.mkordula.opendiabetesassistant.data.metrics.LastDaysDataProviderApi
import info.mkordula.opendiabetesassistant.itemdetails.events.AddFabVisibilityEvent
import info.mkordula.opendiabetesassistant.navigation.MainMenuPresenter
import info.mkordula.opendiabetesassistant.navigation.events.ChangeToolbarNameEvent
import info.mkordula.opendiabetesassistant.utils.AsyncTaskManagerApi
import info.mkordula.opendiabetesassistant.utils.EventBusApi
import info.mkordula.opendiabetesassistant.utils.TestHelper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*

@RunWith(JUnit4::class)
class ChartsPresenterTest {
    private lateinit var mLastDaysDataProvider: LastDaysDataProviderApi
    private lateinit var mEventBusApi: EventBusApi
    private lateinit var mAsyncTaskManagerApi: AsyncTaskManagerApi
    private lateinit var mChartPresenter: ChartsPresenter

    @Before
    fun setUp() {
        mLastDaysDataProvider = mock(LastDaysDataProviderApi::class.java)
        mEventBusApi = mock(EventBusApi::class.java)
        mAsyncTaskManagerApi = object : AsyncTaskManagerApi {
            override fun <T> execute(task: () -> T?, pre: (() -> Unit)?, post: ((result: T?) -> Unit)?) {
                task.invoke()
                post?.invoke(null)
            }
        }
        mChartPresenter = ChartsPresenter(mLastDaysDataProvider, mEventBusApi, mAsyncTaskManagerApi)
    }

    @Test
    fun onDatabaseChanged() {
        // given
        val view = mock(ChartsViewApi::class.java)

        // when
        mChartPresenter.setView(view)
        mChartPresenter.onDatabaseChanged(DatabaseCacheChangedEvent())

        // then
        verifyLoadingData(view)
    }

    private fun verifyLoadingData(view: ChartsViewApi) {
        val orderVerifier = inOrder(view)
        orderVerifier.verify(view).hideCharts()
        orderVerifier.verify(view).showLoading()
        orderVerifier.verify(view).showGlucoseChart(TestHelper.anyKotlin())
        orderVerifier.verify(view).showAverageGlucoseChart(TestHelper.anyKotlin())
        orderVerifier.verify(view).showPrimaryInsulinChart(TestHelper.anyKotlin())
        orderVerifier.verify(view).showSecondaryInsulinChart(TestHelper.anyKotlin())
        orderVerifier.verify(view).showWeightChart(TestHelper.anyKotlin())
        orderVerifier.verify(view).showCharts()
        orderVerifier.verify(view).hideLoading()
    }

    @Test
    fun attachTest() {
        // given
        val view = mock(ChartsViewApi::class.java)

        // when
        mChartPresenter.attach(view)

        // then
        verify(mEventBusApi).postSticky(ChangeToolbarNameEvent(MainMenuPresenter.VIEW.CHARTS))
        verify(mEventBusApi).postSticky(AddFabVisibilityEvent(View.GONE))
        verifyLoadingData(view)
    }
}