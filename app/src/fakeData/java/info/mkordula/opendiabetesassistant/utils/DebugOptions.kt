package info.mkordula.opendiabetesassistant.utils

import info.mkordula.java.database.DaoSession
import info.mkordula.java.database.LogEntryModel
import info.mkordula.opendiabetesassistant.BuildConfig
import java.util.*

class DebugOptions : DebugOptionsApi {
    override fun generateFakeEntries(daoSession: DaoSession) {
        if (BuildConfig.DEBUG) {
            val logEntryModelDao = daoSession.logEntryModelDao

            for (i in 0..4) {
                val logEntryModel = LogEntryModel()
                logEntryModel.date = Date()
                logEntryModel.carbohydratesDose = 1 + i
                logEntryModel.primaryInsulinDose = 5 + i
                logEntryModel.secondaryInsulinDose = 10 + i

                logEntryModelDao.insert(logEntryModel)
            }
        }
    }
}