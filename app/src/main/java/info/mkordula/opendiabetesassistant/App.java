package info.mkordula.opendiabetesassistant;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;

import info.mkordula.opendiabetesassistant.authentication.AuthenticationManager;
import info.mkordula.opendiabetesassistant.authentication.AuthenticationManagerApi;
import info.mkordula.opendiabetesassistant.data.database.DataSnapshotConverter;
import info.mkordula.opendiabetesassistant.data.database.DatabaseDataProvider;
import info.mkordula.opendiabetesassistant.data.database.DatabaseDataProviderApi;


public final class App extends Application {
    private AuthenticationManagerApi mAuthManager;
    private DatabaseDataProviderApi mDataProvider;

    @Override
    public void onCreate() {
        super.onCreate();

        EventBus.builder()
                .throwSubscriberException(BuildConfig.DEBUG)
                .logSubscriberExceptions(BuildConfig.DEBUG)
                .logNoSubscriberMessages(BuildConfig.DEBUG)
                .installDefaultEventBus();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        mAuthManager = new AuthenticationManager();
        mDataProvider = new DatabaseDataProvider(mAuthManager, new DataSnapshotConverter());
    }

    public AuthenticationManagerApi getAuthManager() {
        return mAuthManager;
    }

    public DatabaseDataProviderApi getDataProvider() {
        return mDataProvider;
    }
}
