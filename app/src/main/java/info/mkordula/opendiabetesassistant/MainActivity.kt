package info.mkordula.opendiabetesassistant

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.OnClick
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseUser
import info.mkordula.core.BaseActivity
import info.mkordula.core.BaseViewApi
import info.mkordula.opendiabetesassistant.authentication.AuthenticationManagerApi
import info.mkordula.opendiabetesassistant.charts.ChartsFragment
import info.mkordula.opendiabetesassistant.charts.ChartsPresenter
import info.mkordula.opendiabetesassistant.data.database.DatabaseDataProviderApi
import info.mkordula.opendiabetesassistant.data.metrics.LastDaysDataProvider
import info.mkordula.opendiabetesassistant.itemdetails.ItemAddPresenter
import info.mkordula.opendiabetesassistant.itemdetails.ItemDetailsConfigApi
import info.mkordula.opendiabetesassistant.itemdetails.ItemDetailsFragment
import info.mkordula.opendiabetesassistant.itemdetails.ItemDetailsPresenter
import info.mkordula.opendiabetesassistant.logbook.LogBookFragment
import info.mkordula.opendiabetesassistant.logbook.LogBookPresenter
import info.mkordula.opendiabetesassistant.navigation.MainMenuPresenter
import info.mkordula.opendiabetesassistant.navigation.MainMenuViewApi
import info.mkordula.opendiabetesassistant.statistics.StatsFragment
import info.mkordula.opendiabetesassistant.statistics.StatsPresenter
import info.mkordula.opendiabetesassistant.utils.AsyncTaskManagerWrapper
import info.mkordula.opendiabetesassistant.utils.EventBusWrapper
import info.mkordula.opendiabetesassistant.utils.RowsCollection

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener,
        BaseViewApi<MainMenuPresenter> {
    companion object {
        private const val AUTH_UI_REQUEST_ID = 10
        private const val BUNDLE_FRAGMENT_ID = "fragment-id"
    }

    private val mMainMenuViewApi = object : MainMenuViewApi {
        override fun showCharts() {
            changeFragment({
                val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.CHARTS.fragmentTag)
                        as ChartsFragment? ?: ChartsFragment()
                fragment.setPresenter(mChartsPresenter)

                fragment
            }, MainMenuPresenter.VIEW.CHARTS.fragmentTag)
        }

        override fun getRestoredViewId(): MainMenuPresenter.VIEW = mRestoredViewId

        override fun changeAddFabVisibility(visibility: Int) {
            mFab.visibility = visibility
        }

        override fun changeUser() {
            mDatabaseDataProvider.dispose()
            AuthUI.getInstance()
                    .signOut(this@MainActivity)
                    .addOnCompleteListener({
                        showUsernameAndEmailInMenu()
                        showToastAuthInfo(R.string.auth_info_logout)
                        mAuthManager.triggerAuthUi(this@MainActivity, AUTH_UI_REQUEST_ID)
                    })
        }

        override fun showStats() {
            changeFragment({
                val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.STATS.fragmentTag)
                        as StatsFragment? ?: StatsFragment()
                fragment.setPresenter(mStatsPresenter)

                fragment
            }, MainMenuPresenter.VIEW.STATS.fragmentTag)
        }

        override fun showSettings() {
            Toast.makeText(this@MainActivity, "Generating fake data...", Toast.LENGTH_LONG)
                    .show()
            this@MainActivity.startService(
                    android.content.Intent(this@MainActivity,
                            info.mkordula.opendiabetesassistant.data.FakeDataGenerator::class.java))
        }

        override fun showAdd() {
            changeFragment({
                val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.DETAILS.fragmentTag)
                        as ItemDetailsFragment? ?: ItemDetailsFragment()
                fragment.setPresenter(mAddPresenter)

                fragment
            }, MainMenuPresenter.VIEW.DETAILS.fragmentTag)
        }

        override fun showDetails() {
            changeFragment({
                val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.DETAILS.fragmentTag)
                        as ItemDetailsFragment? ?: ItemDetailsFragment()
                fragment.setPresenter(mDetailsPresenter)

                fragment
            }, MainMenuPresenter.VIEW.DETAILS.fragmentTag)
        }

        override fun showLogBookExcludingBackStack() {
            changeFragment(createLogBookFragment(), MainMenuPresenter.VIEW.LOG.fragmentTag)
        }

        override fun showLogBook() {
            changeFragment(createLogBookFragment(), MainMenuPresenter.VIEW.LOG.fragmentTag)
        }

        private fun createLogBookFragment() = {
            val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.LOG.fragmentTag)
                    as LogBookFragment? ?: LogBookFragment()
            fragment.setPresenter(mLogBookPresenter)

            fragment
        }

        private fun changeFragment(fragmentCreator: () -> Fragment, fragmentTag: String) {
            if (!isFinishing) {
                val fragment = fragmentCreator()
                supportFragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        .replace(R.id.content_main_fragment_holder, fragment, fragmentTag)
                        .commit()
            }
        }

        override fun setToolBarName(viewId: MainMenuPresenter.VIEW) {
            when (viewId) {
                MainMenuPresenter.VIEW.ADD -> mToolbar.title = getString(R.string.drawer_menu_add)
                MainMenuPresenter.VIEW.CHANGE_USER -> mToolbar.title = getString(R.string.drawer_menu_change_user)
                MainMenuPresenter.VIEW.LOG -> mToolbar.title = getString(R.string.drawer_menu_log)
                MainMenuPresenter.VIEW.STATS -> mToolbar.title = getString(R.string.drawer_menu_statistics)
                MainMenuPresenter.VIEW.CHARTS -> mToolbar.title = getString(R.string.drawer_menu_charts)
                MainMenuPresenter.VIEW.SETTINGS -> mToolbar.title = getString(R.string.drawer_menu_settings)
                MainMenuPresenter.VIEW.DETAILS -> mToolbar.title = getString(R.string.drawer_menu_details)
                else -> throw IllegalArgumentException("setToolBarName")
            }
        }
    }

    private fun generateVisibleRowsTypes(): Collection<String> {
        val visibleRows = mutableSetOf<String>()
        mRowsCollection.getRows().forEach({
            visibleRows.add(it.getType().value)
        })
        return visibleRows
    }

    @BindView(R.id.toolbar)
    lateinit var mToolbar: Toolbar

    @BindView(R.id.drawer_layout)
    lateinit var mDrawer: DrawerLayout

    @BindView(R.id.nav_view)
    lateinit var mNavView: NavigationView

    @BindView(R.id.floating_button_quick_add)
    lateinit var mFab: FloatingActionButton

    private lateinit var mDatabaseDataProvider: DatabaseDataProviderApi

    private lateinit var mToggle: ActionBarDrawerToggle
    private lateinit var mMainMenuPresenter: MainMenuPresenter
    private lateinit var mRowsCollection: RowsCollection
    private lateinit var mLogBookPresenter: LogBookPresenter
    private lateinit var mDetailsPresenter: ItemDetailsPresenter
    private lateinit var mAddPresenter: ItemAddPresenter
    private lateinit var mStatsPresenter: StatsPresenter
    private lateinit var mAuthManager: AuthenticationManagerApi
    private lateinit var mChartsPresenter: ChartsPresenter

    private var mRestoredViewId = MainMenuPresenter.VIEW.UNKNOWN

    override fun setPresenter(presenter: MainMenuPresenter?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSetup(savedInstanceState: Bundle?) {
        mToolbar.title = "" // fix for not setting title on startup: https://stackoverflow.com/a/35430590
        setSupportActionBar(mToolbar)
        mToggle = ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        mNavView.setNavigationItemSelectedListener(this)

        if (savedInstanceState != null) {
            restoreData(savedInstanceState)
        }

        initDataLoaders()
        initPresenters()
        mMainMenuPresenter.attach(mMainMenuViewApi)
    }

    private fun restoreData(data: Bundle) {
        mRestoredViewId = MainMenuPresenter.VIEW.valueOf(data.getString(BUNDLE_FRAGMENT_ID, MainMenuPresenter.DEFAULT_VIEW.name))
    }

    private fun initDataLoaders() {
        mRowsCollection = RowsCollection(this.applicationContext)

        val app = application as App
        mAuthManager = app.authManager
        mDatabaseDataProvider = app.dataProvider
    }

    private fun initPresenters() {
        val eventBusApi = EventBusWrapper()
        val asyncTaskManagerApi = AsyncTaskManagerWrapper()

        mMainMenuPresenter = MainMenuPresenter(eventBusApi)

        mDetailsPresenter = ItemDetailsPresenter(object : ItemDetailsConfigApi {
            override fun getVisibleRowsTypes(): Collection<String> {
                return generateVisibleRowsTypes()
            }
        }, mDatabaseDataProvider, mRowsCollection.getRows(), eventBusApi, asyncTaskManagerApi)

        mLogBookPresenter = LogBookPresenter(mDatabaseDataProvider, eventBusApi, asyncTaskManagerApi)

        mAddPresenter = ItemAddPresenter(object : ItemDetailsConfigApi {
            override fun getVisibleRowsTypes(): Collection<String> {
                return generateVisibleRowsTypes()
            }

        }, mDatabaseDataProvider, mRowsCollection.getRows(), eventBusApi, asyncTaskManagerApi)

        val lastDaysDataProviderApi = LastDaysDataProvider(mDatabaseDataProvider)
        mStatsPresenter = StatsPresenter(lastDaysDataProviderApi, eventBusApi, asyncTaskManagerApi)
        mChartsPresenter = ChartsPresenter(lastDaysDataProviderApi, eventBusApi, asyncTaskManagerApi)

        restorePresentersIfNecessary()
    }

    private fun restorePresentersIfNecessary() {
        restoreLogBookPresenter()
        restoreDetailsPresenter()
        restoreAddPresenter()
        restoreStatsPresenter()
        restoreChartsPresenter()
    }

    private fun restoreChartsPresenter() {
        val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.CHARTS.fragmentTag)
        if (fragment != null) {
            (fragment as ChartsFragment).setPresenter(mChartsPresenter)
        }
    }

    private fun restoreStatsPresenter() {
        val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.STATS.fragmentTag)
        if (fragment != null) {
            (fragment as StatsFragment).setPresenter(mStatsPresenter)
        }
    }

    private fun restoreAddPresenter() {
        val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.ADD.fragmentTag)
        if (fragment != null) {
            setPresenterForDetailsView(fragment, mAddPresenter)
        }
    }

    private fun restoreDetailsPresenter() {
        val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.DETAILS.fragmentTag)
        if (fragment != null) {
            setPresenterForDetailsView(fragment, mDetailsPresenter)
        }
    }

    private fun setPresenterForDetailsView(fragment: Fragment?, presenter: ItemDetailsPresenter) {
        (fragment as ItemDetailsFragment).setPresenter(presenter)
    }

    private fun restoreLogBookPresenter() {
        val fragment = supportFragmentManager.findFragmentByTag(MainMenuPresenter.VIEW.LOG.fragmentTag)
        if (fragment != null) {
            (fragment as LogBookFragment).setPresenter(mLogBookPresenter)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun onStart() {
        super.onStart()
        mDrawer.addDrawerListener(mToggle)
        mToggle.syncState()
        if (!mAuthManager.isAuthenticated()) {
            showUsernameAndEmailInMenu()
            mAuthManager.triggerAuthUi(this, AUTH_UI_REQUEST_ID)
        } else {
            mDatabaseDataProvider.goOnline()
            showUsernameAndEmailInMenu(mAuthManager.getUser()!!)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == AUTH_UI_REQUEST_ID) {
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == Activity.RESULT_OK) {
                mDatabaseDataProvider.attachToFirebaseDatabase()
                showUsernameAndEmailInMenu(mAuthManager.getUser()!!)
            } else {
                when {
                    response == null -> showToastAuthInfo(R.string.auth_error_cancelled)
                    response.error?.errorCode == ErrorCodes.NO_NETWORK -> showToastAuthInfo(R.string.auth_error_no_network)
                    else -> showToastAuthInfo(R.string.auth_error_unknown)
                }
            }
        }
    }

    private fun showUsernameAndEmailInMenu(user: FirebaseUser) {
        showUsernameAndEmailInMenu(user.displayName ?: getString(R.string.not_logged_in),
                user.email ?: getString(R.string.n_a))
    }

    private fun showUsernameAndEmailInMenu() {
        showUsernameAndEmailInMenu(getString(R.string.not_logged_in), getString(R.string.n_a))
    }

    private fun showUsernameAndEmailInMenu(username: String, password: String) {
        val headerId = 0
        mNavView.getHeaderView(headerId).findViewById<TextView>(R.id.menuUsername).text = username
        mNavView.getHeaderView(headerId).findViewById<TextView>(R.id.menuEmail).text = password
    }

    private fun showToastAuthInfo(@StringRes stringId: Int) {
        Toast.makeText(this, stringId, Toast.LENGTH_LONG)
                .show()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putString(BUNDLE_FRAGMENT_ID, mMainMenuPresenter.currentSelectedView.name)
        super.onSaveInstanceState(outState)
    }

    override fun onStop() {
        mDrawer.removeDrawerListener(mToggle)
        mDatabaseDataProvider.goOffline()
        super.onStop()
    }

    override fun onDestroy() {
        mMainMenuPresenter.detach()
        super.onDestroy()
    }

    override fun onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START)
        } else if (!supportFragmentManager.isDestroyed
                && !supportFragmentManager.popBackStackImmediate()) {
            if (mMainMenuPresenter.currentSelectedView == MainMenuPresenter.VIEW.LOG) {
                super.onBackPressed()
            } else {
                mMainMenuPresenter.changeView(MainMenuPresenter.VIEW.LOG)
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.drawer_menu_change_user -> mMainMenuPresenter.changeView(MainMenuPresenter.VIEW.CHANGE_USER)
            R.id.drawer_menu_list -> mMainMenuPresenter.changeView(MainMenuPresenter.VIEW.LOG)
            R.id.drawer_menu_settings -> mMainMenuPresenter.changeView(MainMenuPresenter.VIEW.SETTINGS)
            R.id.drawer_menu_stats -> mMainMenuPresenter.changeView(MainMenuPresenter.VIEW.STATS)
            R.id.drawer_menu_charts -> mMainMenuPresenter.changeView(MainMenuPresenter.VIEW.CHARTS)
            else -> throw IllegalArgumentException(item.itemId.toString())
        }

        mDrawer.closeDrawer(GravityCompat.START)
        return true
    }

    @OnClick(R.id.floating_button_quick_add)
    fun onQuickAddClicked() {
        mMainMenuPresenter.changeView(MainMenuPresenter.VIEW.ADD)
    }
}
