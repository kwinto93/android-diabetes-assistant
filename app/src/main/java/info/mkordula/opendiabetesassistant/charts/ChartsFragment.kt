package info.mkordula.opendiabetesassistant.charts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import butterknife.BindView
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import info.mkordula.core.BaseFragment
import info.mkordula.core.BaseViewApi
import info.mkordula.opendiabetesassistant.R
import info.mkordula.opendiabetesassistant.utils.DateToStringConverter
import java.util.*

class ChartsFragment : BaseViewApi<ChartsPresenter>, BaseFragment() {
    private val mViewApi = object : ChartsViewApi {
        override fun showWeightChart(dataChart: List<Entry>) {
            val lineDataSet = LineDataSet(dataChart, getString(R.string.chart_weight_data))
            val description = getString(R.string.chart_weight_description)
            setupChart(mWeightChart, lineDataSet, description)
        }

        override fun showSecondaryInsulinChart(dataChart: List<Entry>) {
            val lineDataSet = LineDataSet(dataChart, getString(R.string.chart_insulin_data))
            val description = getString(R.string.chart_secondary_insulin_description)
            setupChart(mSecondaryInsulinChart, lineDataSet, description)
        }

        override fun showPrimaryInsulinChart(dataChart: List<Entry>) {
            val lineDataSet = LineDataSet(dataChart, getString(R.string.chart_insulin_data))
            val description = getString(R.string.chart_primary_insulin_description)
            setupChart(mPrimaryInsulinChart, lineDataSet, description)
        }

        override fun showAverageGlucoseChart(data: List<Entry>) {
            val lineDataSet = LineDataSet(data, getString(R.string.chart_avg_glucose_data))
            val description = getString(R.string.chart_avg_glucose_description)
            setupChart(mAvgGlucoseChart, lineDataSet, description)
        }

        private val mDateConverter = DateToStringConverter()

        override fun hideCharts() {
            mChartList.visibility = View.GONE
        }

        override fun showLoading() {
            mProgressBar.visibility = View.VISIBLE
        }

        override fun showCharts() {
            mChartList.visibility = View.VISIBLE
        }

        override fun hideLoading() {
            mProgressBar.visibility = View.GONE
        }

        override fun showGlucoseChart(dataSet: List<Entry>) {
            val lineDataSet = LineDataSet(dataSet, getString(R.string.chart_glucose_data))
            val description = getString(R.string.chart_glucose_description)
            setupChart(mGlucoseChart, lineDataSet, description)
        }

        private fun setupChart(chart: LineChart, lineDataSet: LineDataSet, description: String) {
            activity?.runOnUiThread {
                if (lineDataSet.values.isNotEmpty()) {
                    chart.data = LineData(lineDataSet)
                    chart.data.setValueTextSize(resources.getDimension(R.dimen.chart_value_size))
                }
                chart.description = Description().apply { text = description }
                chart.xAxis.labelCount = ChartsPresenter.X_LABELS_COUNT
                chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
                chart.xAxis.valueFormatter = IAxisValueFormatter { value, _ ->
                    mDateConverter.getDateTime(Date(value.toLong()))
                }
                chart.invalidate()
            }
        }

    }

    @BindView(R.id.chart_progress)
    lateinit var mProgressBar: ProgressBar

    @BindView(R.id.chart_list)
    lateinit var mChartList: View

    @BindView(R.id.chart_glucose)
    lateinit var mGlucoseChart: LineChart

    @BindView(R.id.chart_glucose_average_per_day)
    lateinit var mAvgGlucoseChart: LineChart

    @BindView(R.id.chart_primary_insulin)
    lateinit var mPrimaryInsulinChart: LineChart

    @BindView(R.id.chart_secondary_insulin)
    lateinit var mSecondaryInsulinChart: LineChart

    @BindView(R.id.chart_weight)
    lateinit var mWeightChart: LineChart

    private lateinit var mPresenter: ChartsPresenter

    override fun setPresenter(presenter: ChartsPresenter) {
        mPresenter = presenter
    }

    override fun getViewLayout(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater?.inflate(R.layout.charts_fragment, container, false)!!
    }

    override fun onStart() {
        super.onStart()
        mPresenter.attach(mViewApi)
    }

    override fun onStop() {
        mPresenter.detach()
        super.onStop()
    }
}