package info.mkordula.opendiabetesassistant.charts

import com.github.mikephil.charting.data.Entry

interface ChartsViewApi {
    fun hideCharts()
    fun showLoading()
    fun showCharts()
    fun hideLoading()
    fun showGlucoseChart(dataSet: List<Entry>)
    fun showAverageGlucoseChart(data: List<Entry>)
    fun showPrimaryInsulinChart(dataChart: List<Entry>)
    fun showSecondaryInsulinChart(dataChart: List<Entry>)
    fun showWeightChart(dataChart: List<Entry>)
}