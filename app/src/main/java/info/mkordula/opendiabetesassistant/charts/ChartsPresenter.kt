package info.mkordula.opendiabetesassistant.charts

import android.support.annotation.VisibleForTesting
import android.view.View
import com.github.mikephil.charting.data.Entry
import info.mkordula.core.AsyncTaskManager
import info.mkordula.opendiabetesassistant.base.BasePresenter
import info.mkordula.opendiabetesassistant.data.database.events.DatabaseCacheChangedEvent
import info.mkordula.opendiabetesassistant.data.metrics.AverageCalculator
import info.mkordula.opendiabetesassistant.data.metrics.Calculator
import info.mkordula.opendiabetesassistant.data.metrics.LastDaysDataProviderApi
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel
import info.mkordula.opendiabetesassistant.itemdetails.events.AddFabVisibilityEvent
import info.mkordula.opendiabetesassistant.navigation.MainMenuPresenter
import info.mkordula.opendiabetesassistant.navigation.events.ChangeToolbarNameEvent
import info.mkordula.opendiabetesassistant.utils.AsyncTaskManagerApi
import info.mkordula.opendiabetesassistant.utils.EventBusApi
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

class ChartsPresenter(private val mLastDaysDataProvider: LastDaysDataProviderApi,
                      mEventBusApi: EventBusApi,
                      private val mAsyncTaskManagerApi: AsyncTaskManagerApi) : BasePresenter<ChartsViewApi>(mEventBusApi) {
    companion object {
        val X_LABELS_COUNT = 3
    }

    override fun useEventBuss() = true

    @Suppress("UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDatabaseChanged(event: DatabaseCacheChangedEvent) {
        loadingData()
    }

    override fun attach(view: ChartsViewApi) {
        super.attach(view)

        // todo - code duplication - laziness - should be unified later (equals never?) - can be moved to BasePresenter as shared
        mEventBusApi.postSticky(ChangeToolbarNameEvent(MainMenuPresenter.VIEW.CHARTS))
        mEventBusApi.postSticky(AddFabVisibilityEvent(View.GONE))

        loadingData()
    }

    @VisibleForTesting
    fun setView(view: ChartsViewApi) {
        this.view = view
    }

    private fun loadingData() {
        view?.hideCharts()
        view?.showLoading()

        mAsyncTaskManagerApi.execute({
            val logEntries = mLastDaysDataProvider.getDataFromLast30()
            loadGlucoseChart(logEntries)
            loadAverageGlucoseChart(logEntries)
            loadPrimaryInsulinChart(logEntries)
            loadSecondaryInsulinChart(logEntries)
            loadWeightChart(logEntries)
        }, null, {
            view?.showCharts()
            view?.hideLoading()
        })
    }

    private fun loadWeightChart(logEntries: Collection<LogEntryModel>) {
        val dataChart = logEntries.filter { logEntryModel ->
            logEntryModel.weight > 0
        }.reversed().map {
            Entry(it.timeStamp.toFloat(), it.weight.toFloat())
        }
        view?.showWeightChart(dataChart)
    }

    private fun loadSecondaryInsulinChart(logEntries: Collection<LogEntryModel>) {
        val dataChart = logEntries.filter { logEntryModel ->
            logEntryModel.secondaryInsulinDose > 0
        }.reversed().map {
            Entry(it.timeStamp.toFloat(), it.secondaryInsulinDose.toFloat())
        }
        view?.showSecondaryInsulinChart(dataChart)
    }

    private fun loadPrimaryInsulinChart(logEntries: Collection<LogEntryModel>) {
        val dataChart = logEntries.filter { logEntryModel ->
            logEntryModel.primaryInsulinDose > 0
        }.reversed().map {
            Entry(it.timeStamp.toFloat(), it.primaryInsulinDose.toFloat())
        }
        view?.showPrimaryInsulinChart(dataChart)
    }

    private fun loadAverageGlucoseChart(logEntries: Collection<LogEntryModel>) {
        val yesterday = -1
        val daysTimePeriod = 30
        val averageCalculator = AverageCalculator()

        val date = Calendar.getInstance()
        normalizeCalendarToMidnight(date)

        val groupingCalendar = Calendar.getInstance()
        val logEntriesGrouped = logEntries.groupBy {
            groupingCalendar.timeInMillis = it.timeStamp
            groupingCalendar.get(Calendar.DAY_OF_YEAR)
        }

        val data = (0 until daysTimePeriod).map {
            date.add(Calendar.DAY_OF_MONTH, yesterday)
            val dayOfTheYear = date.get(Calendar.DAY_OF_YEAR)

            Pair(Calculator.calculateValue({
                logEntriesGrouped[dayOfTheYear]?.map { logEntryModel ->
                    logEntryModel.glucose.toDouble()
                } ?: emptyList()
            }, averageCalculator), date.timeInMillis)
        }.reversed().map {
            Entry(it.second.toFloat(), it.first.toFloat())
        }
        view?.showAverageGlucoseChart(data)
    }

    private fun normalizeCalendarToMidnight(calendar: Calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
    }

    private fun loadGlucoseChart(logEntries: Collection<LogEntryModel>) {
        val glucoseChartData = logEntries.filter { logEntryModel ->
            logEntryModel.glucose > 0
        }.reversed().map {
            Entry(it.timeStamp.toFloat(), it.glucose.toFloat())
        }
        view?.showGlucoseChart(glucoseChartData)
    }
}