package info.mkordula.opendiabetesassistant.data.database

import android.os.Handler
import android.os.Looper
import android.util.Log
import com.google.firebase.database.*
import info.mkordula.core.AsyncTaskManager
import info.mkordula.opendiabetesassistant.authentication.AuthenticationManagerApi
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel
import info.mkordula.opendiabetesassistant.utils.LogTags
import java.util.concurrent.atomic.AtomicBoolean

class DatabaseDataProvider(private val mAuthenticationManager: AuthenticationManagerApi,
                           mDataSnapshotConverter: DataSnapshotConverter)
    : DatabaseDataProviderApi, ValueEventListener {
    companion object {
        const val BOTTLENECK_DELAY = 1000L
        const val LOG_ENTRIES_DB = "logEntries"
        const val ITEMS_LIMIT = 1000
    }

    private val mDatabaseCache = DatabaseCache(mDataSnapshotConverter)
    private val mBottleneckHandler = Handler(Looper.getMainLooper())
    private val mUpdateShouldBeDeferredAtomic = AtomicBoolean(false)
    private var mDeferredData: DataSnapshot? = null

    init {
        if (mAuthenticationManager.isAuthenticated()) {
            attachToFirebaseDatabase()
        } else {
            logNotAuthError()
        }
    }

    override fun goOnline() {
        getRootDatabase()?.database?.goOnline()
    }

    override fun goOffline() {
        getRootDatabase()?.database?.goOffline()
    }

    override fun onCancelled(error: DatabaseError) {
        logDatabaseError(error)
    }

    override fun onDataChange(data: DataSnapshot) {
        if (!mUpdateShouldBeDeferredAtomic.get()) {
            logDatabaseInfo("onDataChange")
            if (mAuthenticationManager.isAuthenticated()) {
                mDatabaseCache.cacheDataAsync(data)
            } else {
                logNotAuthError()
            }

            mUpdateShouldBeDeferredAtomic.set(true)
            mBottleneckHandler.postDelayed({
                mUpdateShouldBeDeferredAtomic.set(false)
                if (mDeferredData != null) {
                    logDatabaseInfo("handling deferred update...")

                    val deferredData = mDeferredData!!
                    mDeferredData = null
                    onDataChange(deferredData)
                }
            }, BOTTLENECK_DELAY)
        } else {
            logDatabaseInfo("deferring too fast update...")
            mDeferredData = data
        }
    }

    override fun dispose() {
        mDatabaseCache.clear()
    }

    override fun attachToFirebaseDatabase() {
        getRootDatabase()?.removeEventListener(this)
        getRootDatabase()
                ?.limitToLast(ITEMS_LIMIT)
                ?.addValueEventListener(this)
    }

    override fun deleteLogEntrySync(entryId: String) {
        if (mAuthenticationManager.isAuthenticated()) {
            val entity = getRootDatabase()!!.child(entryId)
            entity.removeValue()
        } else {
            logNotAuthError()
        }
    }

    override fun updateLogEntrySync(logEntryModel: LogEntryModel) {
        if (mAuthenticationManager.isAuthenticated()) {
            if (logEntryModel.key != null) {
                val entity = getRootDatabase()!!.child(logEntryModel.key!!)
                entity.setValue(logEntryModel)
            } else {
                logDatabaseError("Root database key is null!")
            }
        } else {
            logNotAuthError()
        }
    }

    override fun saveLogEntrySync(logEntryModel: LogEntryModel) {
        if (mAuthenticationManager.isAuthenticated()) {
            val entity = getRootDatabase()!!.push()
            entity.setValue(logEntryModel)
        } else {
            logNotAuthError()
        }
    }

    override fun getLogEntries() = mDatabaseCache.getImmutableData()

    override fun getLogEntryAsync(callback: (LogEntryModel) -> Unit, entryId: String) {
        if (mAuthenticationManager.isAuthenticated()) {
            AsyncTaskManager.execute({
                getRootDatabase()?.child(entryId)?.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(data: DataSnapshot) {
                        callback(data.getValue(LogEntryModel::class.java)?.apply { key = data.key } ?: LogEntryModel())
                    }

                    override fun onCancelled(error: DatabaseError) {
                        logDatabaseError(error)
                        callback(LogEntryModel())
                    }
                })
            }, null, null)
        } else {
            logNotAuthError()
            callback(LogEntryModel())
        }
    }

    private fun getRootDatabase(): DatabaseReference? {
        return if (mAuthenticationManager.isAuthenticated())
            FirebaseDatabase.getInstance().reference
                    .child(LOG_ENTRIES_DB)
                    .child("${mAuthenticationManager.getUser()?.uid}")
        else null
    }

    private fun logDatabaseError(error: String) {
        Log.e(LogTags.DATABASE.tag, "Database error: $error")
    }

    private fun logDatabaseError(error: DatabaseError?) {
        Log.e(LogTags.DATABASE.tag, "Database error: ${error.toString()}")
    }

    private fun logNotAuthError() {
        Log.w(LogTags.DATABASE.tag, "Not authenticated")
    }

    private fun logDatabaseInfo(text: String) {
        Log.i(LogTags.DATABASE.tag, text)
    }
}