package info.mkordula.opendiabetesassistant.data.metrics

class MinCalculator : CalculatorApi {
    override fun calculate(inputProvider: () -> Collection<Double>): Double {
        val input = inputProvider()
        var min = Double.MAX_VALUE
        input.forEach {
            if (it < min) {
                min = it
            }
        }

        return if (min == Double.MAX_VALUE) 0.0 else min
    }
}