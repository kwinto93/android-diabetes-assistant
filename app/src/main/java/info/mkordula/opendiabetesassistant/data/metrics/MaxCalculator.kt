package info.mkordula.opendiabetesassistant.data.metrics

class MaxCalculator : CalculatorApi {
    override fun calculate(inputProvider: () -> Collection<Double>): Double {
        val input = inputProvider()
        var max = Double.MIN_VALUE
        input.forEach {
            if (it > max) {
                max = it
            }
        }

        return if(max == Double.MIN_VALUE) 0.0 else max
    }
}