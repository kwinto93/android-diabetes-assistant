package info.mkordula.opendiabetesassistant.data.database

import info.mkordula.opendiabetesassistant.data.models.LogEntryModel

interface DatabaseDataProviderApi {
    fun getLogEntries(): Collection<LogEntryModel>
    fun getLogEntryAsync(callback: (LogEntryModel) -> Unit, entryId: String)
    fun saveLogEntrySync(logEntryModel: LogEntryModel)
    fun updateLogEntrySync(logEntryModel: LogEntryModel)
    fun deleteLogEntrySync(entryId: String)
    fun attachToFirebaseDatabase()
    fun dispose()
    fun goOnline()
    fun goOffline()
}