package info.mkordula.opendiabetesassistant.data.metrics

import info.mkordula.opendiabetesassistant.data.database.DatabaseDataProviderApi
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel
import java.util.*

class LastDaysDataProvider(private val mDatabaseDataProvider: DatabaseDataProviderApi)
    : LastDaysDataProviderApi {
    companion object {
        private const val LAST_MONTH = -1
    }

    override fun getDataFromLast30(): Collection<LogEntryModel> {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, LAST_MONTH)

        return mDatabaseDataProvider
                .getLogEntries()
                .filter { logEntryModel ->
                    logEntryModel.timeStamp > calendar.timeInMillis
                }
    }
}