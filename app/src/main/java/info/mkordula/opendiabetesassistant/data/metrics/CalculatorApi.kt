package info.mkordula.opendiabetesassistant.data.metrics

interface CalculatorApi {
    fun calculate(inputProvider: () -> Collection<Double>): Double
}