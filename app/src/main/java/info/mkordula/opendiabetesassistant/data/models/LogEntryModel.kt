package info.mkordula.opendiabetesassistant.data.models

import com.google.firebase.database.Exclude
import java.util.*

class LogEntryModel(
        var timeStamp: Long,
        var glucose: Int = 0,
        var primaryInsulinDose: Int = 0,
        var secondaryInsulinDose: Int = 0,
        var carbohydratesDose: Int = 0,
        var weight: Double = 0.0) {
    constructor() : this(Date().time)

    companion object {
        const val UNKNOWN_ITEM_ID = "UNKNOWN"
        const val FIELD_TIMESTAMP = "timeStamp"
    }

    @Exclude
    var key: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LogEntryModel

        if (timeStamp != other.timeStamp) return false
        if (glucose != other.glucose) return false
        if (primaryInsulinDose != other.primaryInsulinDose) return false
        if (secondaryInsulinDose != other.secondaryInsulinDose) return false
        if (carbohydratesDose != other.carbohydratesDose) return false
        if (weight != other.weight) return false
        if (key != other.key) return false

        return true
    }

    override fun hashCode(): Int {
        var result = timeStamp.hashCode()
        result = 31 * result + glucose
        result = 31 * result + primaryInsulinDose
        result = 31 * result + secondaryInsulinDose
        result = 31 * result + carbohydratesDose
        result = 31 * result + weight.hashCode()
        result = 31 * result + (key?.hashCode() ?: 0)
        return result
    }


}