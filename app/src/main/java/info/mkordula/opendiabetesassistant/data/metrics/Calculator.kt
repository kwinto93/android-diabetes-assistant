package info.mkordula.opendiabetesassistant.data.metrics

object Calculator {
    /**
     * calculates values
     */
    fun calculateValue(inputData: () -> Collection<Double>, calculator: CalculatorApi) = calculator.calculate(inputData)
}