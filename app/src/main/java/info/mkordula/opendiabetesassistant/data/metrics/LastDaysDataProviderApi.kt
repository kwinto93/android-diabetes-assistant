package info.mkordula.opendiabetesassistant.data.metrics

import info.mkordula.opendiabetesassistant.data.models.LogEntryModel

interface LastDaysDataProviderApi {
    fun getDataFromLast30(): Collection<LogEntryModel>
}