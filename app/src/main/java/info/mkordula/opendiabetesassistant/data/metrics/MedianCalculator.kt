package info.mkordula.opendiabetesassistant.data.metrics

import org.apache.commons.math3.stat.descriptive.rank.Median

class MedianCalculator : CalculatorApi {
    override fun calculate(inputProvider: () -> Collection<Double>): Double {
        val input = inputProvider()
        val medianCalculator = Median()
        return medianCalculator.evaluate(input.toDoubleArray())
    }
}