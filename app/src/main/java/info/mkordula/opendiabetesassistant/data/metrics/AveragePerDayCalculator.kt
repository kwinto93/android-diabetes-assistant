package info.mkordula.opendiabetesassistant.data.metrics

class AveragePerDayCalculator(private val mDaysNumber: Int) : AverageCalculator() {
    override fun calculate(inputProvider: () -> Collection<Double>): Double {
        val input = inputProvider()
        return calculateAvg(input, mDaysNumber)
    }
}