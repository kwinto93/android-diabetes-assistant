package info.mkordula.opendiabetesassistant.data.metrics

open class AverageCalculator : CalculatorApi {
    override fun calculate(inputProvider: () -> Collection<Double>): Double {
        val input = inputProvider()
        return calculateAvg(input, input.size)
    }

    protected fun calculateAvg(input: Collection<Double>, avgMetricCount: Int): Double {
        var value = 0.0
        input.forEach {
            value += it
        }

        return value / avgMetricCount
    }
}