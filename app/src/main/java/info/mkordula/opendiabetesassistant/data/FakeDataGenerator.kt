package info.mkordula.opendiabetesassistant.data

import android.app.IntentService
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.support.v4.app.NotificationCompat
import info.mkordula.opendiabetesassistant.authentication.AuthenticationManager
import info.mkordula.opendiabetesassistant.data.database.DataSnapshotConverter
import info.mkordula.opendiabetesassistant.data.database.DatabaseDataProvider
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel
import java.util.*
import java.util.concurrent.TimeUnit

class FakeDataGenerator : IntentService(SERVICE_NAME) {
    private val mUiHandler = Handler(Looper.getMainLooper())

    override fun onHandleIntent(intent: Intent?) {
        val random = Random()
        val currentTime = System.currentTimeMillis()
        val minStepTime = TimeUnit.HOURS.toMillis(2).toInt()
        val maxStepTime = TimeUnit.HOURS.toMillis(6).toInt()
        val minGlucose = 50
        val maxGlucose = 200
        val minCarbohydrates = 0
        val maxCarbohydrates = 6
        val minWeight = 80
        val maxWeight = 90
        val minInsulin = 0
        val maxInsulin = 12

        val carbohydratesPossibility = 0.2f
        val weightPossibility = 0.05f
        val primaryInsulinPossibility = 0.5f
        val secondaryInsulinPossibility = 0.3f
        val glucosePossibility = 0.8f
        val defaultInt = 0
        val defaultDouble = 0.0

        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationBuilder = buildProgressNotification()
        updateProgressOnUiThread(0, notificationManager, notificationBuilder)

        var lastTime = currentTime
        val database = DatabaseDataProvider(AuthenticationManager(), DataSnapshotConverter())
        (0 until ENTRIES_COUNT).forEach {
            val date = lastTime

            val carbohydratesDose = takeActionWhenPossibilityInRange(random, carbohydratesPossibility, {
                minCarbohydrates + random.nextInt(maxCarbohydrates - minCarbohydrates)
            }, defaultInt)
            val glucose = takeActionWhenPossibilityInRange(random, glucosePossibility, {
                minGlucose + random.nextInt(maxGlucose - minGlucose)
            }, defaultInt)
            val primaryInsulinDose = takeActionWhenPossibilityInRange(random, primaryInsulinPossibility, {
                minInsulin + random.nextInt(maxInsulin - minInsulin)
            }, defaultInt)
            val secondaryInsulinDose = takeActionWhenPossibilityInRange(random, secondaryInsulinPossibility, {
                minInsulin + random.nextInt(maxInsulin - minInsulin)
            }, defaultInt)
            val weight = takeActionWhenPossibilityInRange(random, weightPossibility, {
                minWeight + random.nextInt(maxWeight - minWeight).toDouble()
            }, defaultDouble)

            val logEntry = LogEntryModel(date, glucose, primaryInsulinDose, secondaryInsulinDose, carbohydratesDose, weight)
            database.saveLogEntrySync(logEntry)

            lastTime -= (minStepTime + random.nextInt(maxStepTime - minStepTime))
            updateProgressOnUiThread(it, notificationManager, notificationBuilder)
        }
    }

    private fun <T> takeActionWhenPossibilityInRange(random: Random, range: Float, action: () -> T, defaultValue: T): T {
        if (random.nextFloat() <= range) return action()
        return defaultValue
    }

    private fun buildProgressNotification(): NotificationCompat.Builder = NotificationCompat.Builder(applicationContext, SERVICE_NAME)
            .setContentTitle("Generating fake data for Diabetes Assistant")
            .setContentText("In progress...")
            .setSmallIcon(android.R.drawable.ic_menu_info_details)
            .setAutoCancel(false)

    private fun updateProgressOnUiThread(progress: Int, notificationManager: NotificationManager,
                                         notificationBuilder: NotificationCompat.Builder) {
        dispatchOnUiThread {
            updateProgress(progress, notificationManager, notificationBuilder)
        }
    }

    private fun updateProgress(progress: Int, notificationManager: NotificationManager,
                               notificationBuilder: NotificationCompat.Builder) {
        if (progress < ENTRIES_COUNT - 1) {
            notificationBuilder.setProgress(ENTRIES_COUNT, progress, false)
        } else {
            notificationBuilder.setContentText("Completed")
        }

        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build())
    }

    private fun dispatchOnUiThread(job: () -> Unit) = mUiHandler.post(job)

    companion object {
        const val SERVICE_NAME = "FakeDataGenerator"
        const val ENTRIES_COUNT = 1000
        const val NOTIFICATION_ID = 1
    }
}