package info.mkordula.opendiabetesassistant.data.database

import com.google.firebase.database.DataSnapshot
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel

class DataSnapshotConverter : DataSnapshotConverterApi {
    override fun convert(data: DataSnapshot): Collection<LogEntryModel> {
        // Firebase is storing data in ascending order, so the latest item is the last on the list
        return data.children.map {
            it.getValue(LogEntryModel::class.java)!!.apply {
                key = it.key
            }
        }.reversed()
    }
}