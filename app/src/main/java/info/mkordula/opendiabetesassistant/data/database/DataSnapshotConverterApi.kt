package info.mkordula.opendiabetesassistant.data.database

import com.google.firebase.database.DataSnapshot
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel

interface DataSnapshotConverterApi {
    fun convert(data: DataSnapshot): Collection<LogEntryModel>
}