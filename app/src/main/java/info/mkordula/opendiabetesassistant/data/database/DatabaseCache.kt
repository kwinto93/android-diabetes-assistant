package info.mkordula.opendiabetesassistant.data.database

import android.util.Log
import com.google.firebase.database.DataSnapshot
import info.mkordula.core.AsyncTaskManager
import info.mkordula.opendiabetesassistant.data.database.events.DatabaseCacheChangedEvent
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel
import info.mkordula.opendiabetesassistant.utils.LogTags
import org.greenrobot.eventbus.EventBus

class DatabaseCache(private val mDataSnapshotConverter: DataSnapshotConverter) {
    private val mCachedEntries = mutableListOf<LogEntryModel>()

    fun getImmutableData(): Collection<LogEntryModel> {
        mCachedEntries.sortByDescending { logEntryModel -> logEntryModel.timeStamp }
        return mCachedEntries.toList()
    }

    /**
     * Thread-safe, synced on the instance of the class
     */
    fun cacheDataAsync(data: DataSnapshot?) {
        AsyncTaskManager.execute({
            synchronized(this@DatabaseCache) {
                if (data == null) {
                    logError("Data snapshot is null, aborting update of cache")
                    return@execute
                }

                val entries = mDataSnapshotConverter.convert(data)
                mCachedEntries.clear()
                if (!mCachedEntries.addAll(entries)) {
                    logError("Log entries cannot be set")
                }
            }
        }, null, {
            EventBus.getDefault().post(DatabaseCacheChangedEvent())
        })
    }

    private fun logError(text: String) {
        Log.e(LogTags.DATABASE.tag, text)
    }

    fun clear() {
        mCachedEntries.clear()
    }
}