package info.mkordula.opendiabetesassistant.base

import info.mkordula.core.BasePresenterApi
import info.mkordula.opendiabetesassistant.utils.EventBusApi
import org.greenrobot.eventbus.EventBus

abstract class BasePresenter<V : Any>(protected val mEventBusApi: EventBusApi) : BasePresenterApi<V> {
    var view: V? = null
        protected set

    override fun attach(view: V) {
        this.view = view
        if (useEventBuss()) {
            mEventBusApi.register(this)
        }
    }

    override fun detach() {
        if (useEventBuss()) {
            mEventBusApi.unregister(this)
        }
        view = null
    }

    abstract protected fun useEventBuss(): Boolean
}