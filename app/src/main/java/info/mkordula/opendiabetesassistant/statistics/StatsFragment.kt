package info.mkordula.opendiabetesassistant.statistics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import butterknife.BindView
import info.mkordula.core.BaseFragment
import info.mkordula.core.BaseViewApi
import info.mkordula.opendiabetesassistant.R

class StatsFragment : BaseViewApi<StatsPresenter>, BaseFragment() {
    private val mViewApi = object : StatsViewApi {
        override fun hideList() {
            mContainer.visibility = View.GONE
        }

        override fun showList() {
            mContainer.visibility = View.VISIBLE
        }

        override fun showLoading() {
            mProgressBar.visibility = View.VISIBLE
        }

        override fun hideLoading() {
            mProgressBar.visibility = View.GONE
        }

        override fun showAverageGlucose(value: String) {
            activity?.runOnUiThread {
                mAverageGlucose.showValue(value)
                mAverageGlucose.showUnit(R.string.unit_mg_dl)
            }
        }

        override fun showMedianGlucose(value: String) {
            activity?.runOnUiThread {
                mMedianGlucose.showValue(value)
                mMedianGlucose.showUnit(R.string.unit_mg_dl)
            }
        }

        override fun showMinGlucose(value: String) {
            activity?.runOnUiThread {
                mMinGlucose.showValue(value)
                mMinGlucose.showUnit(R.string.unit_mg_dl)
            }
        }

        override fun showMaxGlucose(value: String) {
            activity?.runOnUiThread {
                mMaxGlucose.showValue(value)
                mMaxGlucose.showUnit(R.string.unit_mg_dl)
            }
        }

        override fun showAveragePrimaryInsulin(value: String) {
            activity?.runOnUiThread {
                mAveragePrimaryInsulin.showValue(value)
                mAveragePrimaryInsulin.showUnit(R.string.unit_u)
            }
        }

        override fun showAverageSecondaryInsulin(value: String) {
            activity?.runOnUiThread {
                mAverageSecondaryInsulin.showValue(value)
                mAverageSecondaryInsulin.showUnit(R.string.unit_u)
            }
        }

        override fun showAverageWeight(value: String) {
            activity?.runOnUiThread {
                mAverageWeight.showValue(value)
                mAverageWeight.showUnit(R.string.unit_kg)
            }
        }

        override fun showAverageCarbohydrates(value: String) {
            activity?.runOnUiThread {
                mAverageCarbohydrates.showValue(value)
                mAverageCarbohydrates.showUnit(R.string.unit_ww)
            }
        }

        override fun showCarbohydratesPerDay(value: String) {
            activity?.runOnUiThread {
                mCarbohydratesPerDay.showValue(value)
                mCarbohydratesPerDay.showUnit(R.string.unit_ww)
            }
        }

        override fun showAverageReadingsPerDay(value: String) {
            activity?.runOnUiThread {
                mAverageReadingsPerDay.showValue(value)
            }
        }

        override fun setLabels() {
            mAverageGlucose.mLabel.text = getString(R.string.stats_label_average_glucose)
            mMedianGlucose.mLabel.text = getString(R.string.stats_label_median_glucose)
            mMinGlucose.mLabel.text = getString(R.string.stats_label_min_glucose)
            mMaxGlucose.mLabel.text = getString(R.string.stats_label_max_glucose)
            mAveragePrimaryInsulin.mLabel.text = getString(R.string.stats_label_average_primary_insulin)
            mAverageSecondaryInsulin.mLabel.text = getString(R.string.stats_label_average_secondary_insulin)
            mAverageWeight.mLabel.text = getString(R.string.stats_label_average_weight)
            mAverageCarbohydrates.mLabel.text = getString(R.string.stats_label_average_carbohydrates)
            mCarbohydratesPerDay.mLabel.text = getString(R.string.stats_label_carbohydrates_per_day)
            mAverageReadingsPerDay.mLabel.text = getString(R.string.stats_label_average_readings_per_day)
        }
    }

    @BindView(R.id.stats_average_glucose)
    lateinit var mAverageGlucose: StatsRowView

    @BindView(R.id.stats_median_glucose)
    lateinit var mMedianGlucose: StatsRowView

    @BindView(R.id.stats_min_glucose)
    lateinit var mMinGlucose: StatsRowView

    @BindView(R.id.stats_max_glucose)
    lateinit var mMaxGlucose: StatsRowView

    @BindView(R.id.stats_average_primary_insulin)
    lateinit var mAveragePrimaryInsulin: StatsRowView

    @BindView(R.id.stats_average_secondary_insulin)
    lateinit var mAverageSecondaryInsulin: StatsRowView

    @BindView(R.id.stats_average_weight)
    lateinit var mAverageWeight: StatsRowView

    @BindView(R.id.stats_average_carbohydrates)
    lateinit var mAverageCarbohydrates: StatsRowView

    @BindView(R.id.stats_carbohydrates_per_day)
    lateinit var mCarbohydratesPerDay: StatsRowView

    @BindView(R.id.stats_average_readings_per_day)
    lateinit var mAverageReadingsPerDay: StatsRowView

    @BindView(R.id.stats_progress)
    lateinit var mProgressBar: ProgressBar

    @BindView(R.id.stats_container)
    lateinit var mContainer: View

    private lateinit var mPresenter: StatsPresenter

    override fun setPresenter(presenter: StatsPresenter) {
        mPresenter = presenter
    }

    override fun getViewLayout(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View
            = inflater?.inflate(R.layout.stats_fragment, container, false)!!

    override fun onStart() {
        super.onStart()
        mPresenter.attach(mViewApi)
    }

    override fun onStop() {
        mPresenter.detach()
        super.onStop()
    }
}