package info.mkordula.opendiabetesassistant.statistics

import android.content.Context
import android.support.annotation.StringRes
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import info.mkordula.opendiabetesassistant.R

class StatsRowView : FrameLayout {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    @BindView(R.id.stats_row_label)
    lateinit var mLabel: TextView

    @BindView(R.id.stats_row_value)
    lateinit var mValue: TextView

    @BindView(R.id.stats_row_unit)
    lateinit var mUnit: TextView

    init {
        inflate(context, R.layout.stats_row, this)
        ButterKnife.bind(this)
    }

    fun showValue(value: String) {
        mValue.text = value
    }

    fun showUnit(@StringRes unit: Int) {
        mUnit.text = context.getText(unit)
    }
}