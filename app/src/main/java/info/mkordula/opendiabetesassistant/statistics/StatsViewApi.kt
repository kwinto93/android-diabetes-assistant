package info.mkordula.opendiabetesassistant.statistics

interface StatsViewApi {
    fun setLabels()
    fun showAverageGlucose(value: String)
    fun showMedianGlucose(value: String)
    fun showMinGlucose(value: String)
    fun showMaxGlucose(value: String)
    fun showAveragePrimaryInsulin(value: String)
    fun showAverageSecondaryInsulin(value: String)
    fun showAverageWeight(value: String)
    fun showAverageCarbohydrates(value: String)
    fun showCarbohydratesPerDay(value: String)
    fun showAverageReadingsPerDay(value: String)
    fun showLoading()
    fun hideLoading()
    fun hideList()
    fun showList()
}