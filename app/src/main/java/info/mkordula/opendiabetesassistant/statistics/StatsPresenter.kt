package info.mkordula.opendiabetesassistant.statistics

import android.view.View
import info.mkordula.core.AsyncTaskManager
import info.mkordula.opendiabetesassistant.base.BasePresenter
import info.mkordula.opendiabetesassistant.data.database.events.DatabaseCacheChangedEvent
import info.mkordula.opendiabetesassistant.data.metrics.*
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel
import info.mkordula.opendiabetesassistant.itemdetails.events.AddFabVisibilityEvent
import info.mkordula.opendiabetesassistant.navigation.MainMenuPresenter
import info.mkordula.opendiabetesassistant.navigation.events.ChangeToolbarNameEvent
import info.mkordula.opendiabetesassistant.utils.AsyncTaskManagerApi
import info.mkordula.opendiabetesassistant.utils.EventBusApi
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

class StatsPresenter(private val mLastDaysDataProvider: LastDaysDataProviderApi,
                     mEventBusApi: EventBusApi,
                     private val mAsyncTaskManagerApi: AsyncTaskManagerApi)
    : BasePresenter<StatsViewApi>(mEventBusApi) {
    companion object {
        private const val VALUE_FORMAT = "%.2f"
    }

    override fun useEventBuss(): Boolean = true

    override fun attach(view: StatsViewApi) {
        super.attach(view)

        mEventBusApi.postSticky(ChangeToolbarNameEvent(MainMenuPresenter.VIEW.STATS))
        mEventBusApi.postSticky(AddFabVisibilityEvent(View.VISIBLE))

        setLabels()
        loadData()
    }

    @Suppress("UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDatabaseChanged(event: DatabaseCacheChangedEvent) {
        loadData()
    }

    private fun loadData() {
        view?.showLoading()
        view?.hideList()

        mAsyncTaskManagerApi.execute({
            val logEntries = mLastDaysDataProvider.getDataFromLast30()

            @Suppress("DEPRECATION")
            val notEmptyDaysCount = logEntries.map {
                val localDate = Date(it.timeStamp)
                localDate.year + localDate.month + localDate.date
            }.distinct().size

            val averageCalculator = AverageCalculator()
            val averagePerDayCalculator = AveragePerDayCalculator(notEmptyDaysCount)
            val medianCalculator = MedianCalculator()
            val minCalculator = MinCalculator()
            val maxCalculator = MaxCalculator()

            // glucose
            val glucoseMap = fun(data: Collection<LogEntryModel>) = {
                data.filter { logEntryModel ->
                    logEntryModel.glucose > 0.0
                }.map {
                    it.glucose.toDouble()
                }
            }

            view?.showAverageGlucose(VALUE_FORMAT.format(Calculator.calculateValue({
                glucoseMap(logEntries).invoke()
            }, averageCalculator)))

            view?.showMedianGlucose(VALUE_FORMAT.format(Calculator.calculateValue({
                glucoseMap(logEntries).invoke()
            }, medianCalculator)))

            view?.showMinGlucose(VALUE_FORMAT.format(Calculator.calculateValue({
                glucoseMap(logEntries).invoke()
            }, minCalculator)))

            view?.showMaxGlucose(VALUE_FORMAT.format(Calculator.calculateValue({
                glucoseMap(logEntries).invoke()
            }, maxCalculator)))

            // insulin
            view?.showAveragePrimaryInsulin(VALUE_FORMAT.format(Calculator.calculateValue({
                logEntries.filter { logEntryModel ->
                    logEntryModel.primaryInsulinDose > 0
                }.map {
                    it.primaryInsulinDose.toDouble()
                }
            }, averageCalculator)))

            view?.showAverageSecondaryInsulin(VALUE_FORMAT.format(Calculator.calculateValue({
                logEntries.filter { logEntryModel ->
                    logEntryModel.secondaryInsulinDose > 0
                }.map {
                    it.secondaryInsulinDose.toDouble()
                }
            }, averageCalculator)))

            // weight
            view?.showAverageWeight(VALUE_FORMAT.format(Calculator.calculateValue({
                logEntries.filter { logEntryModel ->
                    logEntryModel.weight > 0
                }.map {
                    it.weight
                }
            }, averageCalculator)))

            // carbohydrates avg
            val carbohydratesMap = fun(data: Collection<LogEntryModel>) = {
                data.filter { logEntryModel ->
                    logEntryModel.carbohydratesDose > 0
                }.map {
                    it.carbohydratesDose.toDouble()
                }
            }
            view?.showAverageCarbohydrates(VALUE_FORMAT.format(Calculator.calculateValue({
                carbohydratesMap(logEntries).invoke()
            }, averageCalculator)))

            // carbohydrates per day
            view?.showCarbohydratesPerDay(VALUE_FORMAT.format(Calculator.calculateValue({
                carbohydratesMap(logEntries).invoke()
            }, averagePerDayCalculator)))

            // readings per day
            view?.showAverageReadingsPerDay(VALUE_FORMAT.format(Calculator.calculateValue({
                listOf(logEntries.filter {
                    it.glucose > 0
                }.size.toDouble())
            }, averagePerDayCalculator)))
        }, null, {
            view?.hideLoading()
            view?.showList()
        })
    }

    private fun setLabels() = view?.setLabels()
}