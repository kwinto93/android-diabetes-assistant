package info.mkordula.opendiabetesassistant.itemdetails

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import info.mkordula.core.BaseFragment
import info.mkordula.core.BaseViewApi
import info.mkordula.opendiabetesassistant.R
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel
import info.mkordula.opendiabetesassistant.navigation.events.ShowMainViewEvent
import info.mkordula.opendiabetesassistant.utils.DateToStringConverter
import info.mkordula.opendiabetesassistant.utils.RowsCollection
import org.greenrobot.eventbus.EventBus
import java.util.*

class ItemDetailsFragment : BaseViewApi<ItemDetailsPresenter>, BaseFragment() {
    companion object {
        private const val BUNDLE_ENTRY_ID = "bundle-entry-id"
        private const val BUNDLE_ENTRY_TIMESTAMP = "bundle-entry-timestamp"
    }

    private val mViewApi = object : ItemDetailsViewApi {
        override fun getCachedLogEntryTimeStamp() = mCachedEntryTimestamp

        override fun getCachedLogEntryId() = mCachedEntryId

        override fun isReceivedBundle() = mReceivedBundle

        override fun showFieldsValidationError() {
            AlertDialog.Builder(activity)
                    .setTitle(R.string.validation_error_title)
                    .setMessage(R.string.validation_error_content)
                    .create()
                    .show()
        }

        override fun hideProgressBar() {
            mProgressBar.visibility = View.GONE
            mDetailsList.visibility = View.VISIBLE
        }

        override fun showProgressBar() {
            mProgressBar.visibility = View.VISIBLE
            mDetailsList.visibility = View.GONE
        }

        override fun showDetails(rows: Collection<ItemDetailsRowApi>) {
            val rowsIterator = rows.iterator()

            mDateRow.text = rowsIterator.next().getValue()
            mGlucoseRow.setText(rowsIterator.next().getValue())
            mPrimaryInsulinRow.setText(rowsIterator.next().getValue())
            mSecondaryInsulinRow.setText(rowsIterator.next().getValue())
            mCarbohydratesRow.setText(rowsIterator.next().getValue())
            mWeightRow.setText(rowsIterator.next().getValue())
        }
    }

    private val mDateToStringConverter = DateToStringConverter()

    @BindView(R.id.details_progressbar)
    lateinit var mProgressBar: ProgressBar
    @BindView(R.id.details_list)
    lateinit var mDetailsList: LinearLayout
    @BindView(R.id.details_row_date)
    lateinit var mDateRow: TextView
    @BindView(R.id.details_row_glucose)
    lateinit var mGlucoseRow: EditText
    @BindView(R.id.details_row_primary_insulin)
    lateinit var mPrimaryInsulinRow: EditText
    @BindView(R.id.details_row_secondary_insulin)
    lateinit var mSecondaryInsulinRow: EditText
    @BindView(R.id.details_row_carbohydrates)
    lateinit var mCarbohydratesRow: EditText
    @BindView(R.id.details_row_weight)
    lateinit var mWeightRow: EditText

    private lateinit var mPresenter: ItemDetailsPresenter

    private var mReceivedBundle = false
    private var mCachedEntryId = LogEntryModel.UNKNOWN_ITEM_ID
    private var mCachedEntryTimestamp = Calendar.getInstance().timeInMillis

    override fun setPresenter(presenter: ItemDetailsPresenter) {
        mPresenter = presenter
    }

    override fun getViewLayout(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater?.inflate(R.layout.details_content, container, false)!!
    }

    override fun onSetup(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            mReceivedBundle = true
            mCachedEntryId = savedInstanceState.getString(BUNDLE_ENTRY_ID, LogEntryModel.UNKNOWN_ITEM_ID)
            mCachedEntryTimestamp = savedInstanceState.getLong(BUNDLE_ENTRY_TIMESTAMP, mPresenter.calendar.timeInMillis)
        }
    }

    override fun onStart() {
        super.onStart()
        mPresenter.attach(mViewApi)

        if (mDateRow.text == null || mDateRow.text == RowsCollection.EMPTY_TEXT) {
            mDateRow.text = mDateToStringConverter.getDateTime(mPresenter.calendar.time)
        }
    }

    override fun onStop() {
        mPresenter.detach()
        super.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(BUNDLE_ENTRY_ID, mPresenter.cachedLogEntryId)
        outState.putLong(BUNDLE_ENTRY_TIMESTAMP, mPresenter.calendar.timeInMillis)
        super.onSaveInstanceState(outState)
    }

    @OnClick(R.id.add_ok)
    fun onOkClicked() {
        val glucose: Int? = getEditTextValue(mGlucoseRow).toIntOrNull()
        val primaryDose: Int? = getEditTextValue(mPrimaryInsulinRow).toIntOrNull()
        val secondaryDose: Int? = getEditTextValue(mSecondaryInsulinRow).toIntOrNull()
        val carbohydrates: Int? = getEditTextValue(mCarbohydratesRow).toIntOrNull()
        val weight: Double? = getEditTextValue(mWeightRow).toDoubleOrNull()

        mPresenter.saveLogEntry(
                glucose ?: 0,
                primaryDose ?: 0,
                secondaryDose ?: 0,
                carbohydrates ?: 0,
                weight ?: 0.0)
        hideKeyboard()
    }

    private fun hideKeyboard() {
        val inputManager: InputMethodManager? = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager?.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)
    }

    private fun getEditTextValue(editText: EditText) = editText.text.toString()

    @OnClick(R.id.add_cancel)
    fun onCancelClicked() {
        hideKeyboard()
        EventBus.getDefault().post(ShowMainViewEvent())
    }

    @OnClick(R.id.details_row_date_button)
    fun onDateButtonClicked() {
        val calendar = mPresenter.calendar
        @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
        DatePickerDialog(context, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, day)

            TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                calendar.set(Calendar.HOUR_OF_DAY, hour)
                calendar.set(Calendar.MINUTE, minute)

                mPresenter.updateDate(calendar)
                mDateRow.text = mDateToStringConverter.getDateTime(calendar.time)
            },
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE),
                    DateFormat.is24HourFormat(context))
                    .show()
        },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))
                .show()
    }
}