package info.mkordula.opendiabetesassistant.itemdetails

import android.view.View
import info.mkordula.core.AsyncTaskManager
import info.mkordula.opendiabetesassistant.base.BasePresenter
import info.mkordula.opendiabetesassistant.data.database.DatabaseDataProviderApi
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel.Companion.UNKNOWN_ITEM_ID
import info.mkordula.opendiabetesassistant.itemdetails.events.AddFabVisibilityEvent
import info.mkordula.opendiabetesassistant.itemdetails.events.ShowDetailsEvent
import info.mkordula.opendiabetesassistant.navigation.MainMenuPresenter
import info.mkordula.opendiabetesassistant.navigation.events.ChangeToolbarNameEvent
import info.mkordula.opendiabetesassistant.navigation.events.ChangeViewEvent
import info.mkordula.opendiabetesassistant.utils.AsyncTaskManagerApi
import info.mkordula.opendiabetesassistant.utils.DateToStringConverter
import info.mkordula.opendiabetesassistant.utils.EventBusApi
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*

open class ItemDetailsPresenter(private val mItemDetailsConfigApi: ItemDetailsConfigApi,
                                private val mDatabaseDataProviderApi: DatabaseDataProviderApi,
                                private val mRowsCollection: Collection<ItemDetailsRowApi>,
                                mEventBusApi: EventBusApi,
                                private val mAsyncTaskManagerApi: AsyncTaskManagerApi)
    : BasePresenter<ItemDetailsViewApi>(mEventBusApi) {
    private val mDateToStringConverter = DateToStringConverter()

    var cachedLogEntryId = UNKNOWN_ITEM_ID
        private set
    var calendar: Calendar = Calendar.getInstance()
        private set

    override fun useEventBuss() = true

    override fun attach(view: ItemDetailsViewApi) {
        super.attach(view)
        mEventBusApi.postSticky(ChangeToolbarNameEvent(getType()))
        mEventBusApi.postSticky(AddFabVisibilityEvent(View.GONE))

        // configuration change
        when {
            view.isReceivedBundle() -> {
                cachedLogEntryId = view.getCachedLogEntryId()
                calendar.timeInMillis = view.getCachedLogEntryTimeStamp()
                view.hideProgressBar()
            }
            cachedLogEntryId != UNKNOWN_ITEM_ID -> // back from recent
                view.hideProgressBar()
            else -> // first launch
                showLogEntryById(cachedLogEntryId)
        }
    }

    open protected fun getType(): MainMenuPresenter.VIEW = MainMenuPresenter.VIEW.DETAILS

    @Suppress("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onShowDetails(event: ShowDetailsEvent) {
        mEventBusApi.removeStickyEvent(event)

        cachedLogEntryId = event.logEntryId
        showLogEntryById(cachedLogEntryId)
    }

    private fun showLogEntryById(id: String) {
        when {
        // newly opened
            id != UNKNOWN_ITEM_ID -> mDatabaseDataProviderApi.getLogEntryAsync({
                fillRowsAndShowDataAsync(it)
            }, id)
        // empty view - Add View
            else -> showDetails(collectRows())
        }
    }

    private fun fillRowsAndShowDataAsync(logEntryModel: LogEntryModel) {
        view?.showProgressBar()
        calendar.timeInMillis = logEntryModel.timeStamp
        mAsyncTaskManagerApi.execute<Collection<ItemDetailsRowApi>>({
            fillRowsByData(collectRows(), logEntryModel)
        }, null, {
            showDetails(it!!)
        })
    }

    private fun showDetails(rows: Collection<ItemDetailsRowApi>) {
        view?.hideProgressBar()
        view?.showDetails(rows)
    }

    private fun collectRows(): Collection<ItemDetailsRowApi> {
        val rows = mutableSetOf<ItemDetailsRowApi>()
        val allowedRows = mItemDetailsConfigApi.getVisibleRowsTypes()

        mRowsCollection.forEach {
            if (allowedRows.contains(it.getType().value)) {
                rows.add(ItemDetailsRow(it, true))
            }
        }

        return rows
    }

    private fun fillRowsByData(rows: Collection<ItemDetailsRowApi>, logEntryModel: LogEntryModel)
            : Collection<ItemDetailsRowApi> {
        rows.forEach({
            when (it.getType()) {
                ItemDetailsRowApi.DetailType.DATE_TIME -> it.setValue(mDateToStringConverter.getDateTime(Date(logEntryModel.timeStamp)))
                ItemDetailsRowApi.DetailType.GLUCOSE -> it.setValue(logEntryModel.glucose.toString())
                ItemDetailsRowApi.DetailType.PRIMARY_INSULIN -> it.setValue(logEntryModel.primaryInsulinDose.toString())
                ItemDetailsRowApi.DetailType.SECONDARY_INSULIN -> it.setValue(logEntryModel.secondaryInsulinDose.toString())
                ItemDetailsRowApi.DetailType.CARBOHYDRATES_DOSE -> it.setValue(logEntryModel.carbohydratesDose.toString())
                ItemDetailsRowApi.DetailType.WEIGHT -> it.setValue(logEntryModel.weight.toString())
            }
        })

        return rows
    }

    fun saveLogEntry(glucose: Int, primaryInsulin: Int, secondaryInsulin: Int,
                     carbohydratesDose: Int, weight: Double) {
        val saveLogEntry = LogEntryModel()
        saveLogEntry.key = cachedLogEntryId

        if (fieldsInvalid(glucose, primaryInsulin, secondaryInsulin, carbohydratesDose, weight)) {
            view?.showFieldsValidationError()
        } else {
            saveLogEntry.timeStamp = calendar.timeInMillis
            saveLogEntry.glucose = glucose
            saveLogEntry.primaryInsulinDose = primaryInsulin
            saveLogEntry.secondaryInsulinDose = secondaryInsulin
            saveLogEntry.carbohydratesDose = carbohydratesDose
            saveLogEntry.weight = weight

            if (saveLogEntry.key == UNKNOWN_ITEM_ID) {
                mDatabaseDataProviderApi.saveLogEntrySync(saveLogEntry)
            } else {
                mDatabaseDataProviderApi.updateLogEntrySync(saveLogEntry)
            }

            EventBus.getDefault().post(ChangeViewEvent(MainMenuPresenter.VIEW.LOG))
        }
    }

    private fun fieldsInvalid(glucose: Int, primaryInsulin: Int, secondaryInsulin: Int, carbohydratesDose: Int, weight: Double) =
            glucose < 0 || primaryInsulin < 0 || secondaryInsulin < 0 || carbohydratesDose < 0 || weight < 0

    fun updateDate(calendar: Calendar) {
        this.calendar = calendar
    }
}