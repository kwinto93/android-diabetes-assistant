package info.mkordula.opendiabetesassistant.itemdetails.events

data class AddFabVisibilityEvent(val visibility: Int)