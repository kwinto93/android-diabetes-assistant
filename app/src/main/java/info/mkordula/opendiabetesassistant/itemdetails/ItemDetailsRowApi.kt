package info.mkordula.opendiabetesassistant.itemdetails

interface ItemDetailsRowApi {
    fun getType(): DetailType
    fun isVisible(): Boolean
    fun getLabel(): String
    fun setLabel(label: String)
    fun getValue(): String
    fun setValue(value: String)

    enum class DetailType(val value: String) {
        DATE_TIME("dateTime"),
        GLUCOSE("glucose"),
        PRIMARY_INSULIN("primary_insulin"),
        SECONDARY_INSULIN("secondary_insulin"),
        CARBOHYDRATES_DOSE("carbohydrates_dose"),
        WEIGHT("weight")
    }
}