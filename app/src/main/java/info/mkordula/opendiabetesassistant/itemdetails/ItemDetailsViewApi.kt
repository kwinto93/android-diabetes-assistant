package info.mkordula.opendiabetesassistant.itemdetails

interface ItemDetailsViewApi {
    fun showDetails(rows: Collection<ItemDetailsRowApi>)
    fun hideProgressBar()
    fun showProgressBar()
    fun showFieldsValidationError()
    fun isReceivedBundle(): Boolean
    fun getCachedLogEntryId(): String
    fun getCachedLogEntryTimeStamp(): Long
}