package info.mkordula.opendiabetesassistant.itemdetails

interface ItemDetailsConfigApi {
    fun getVisibleRowsTypes(): Collection<String>
}