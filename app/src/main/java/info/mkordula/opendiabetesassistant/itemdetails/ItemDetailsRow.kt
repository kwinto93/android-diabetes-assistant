package info.mkordula.opendiabetesassistant.itemdetails

class ItemDetailsRow(private val mType: ItemDetailsRowApi.DetailType,
                     private var mLabel: String,
                     private var mValue: String,
                     private var mIsVisible: Boolean = false) : ItemDetailsRowApi {
    constructor(copy: ItemDetailsRowApi, isVisible: Boolean = false)
            : this(copy.getType(), copy.getLabel(), copy.getValue(), isVisible)

    override fun getType(): ItemDetailsRowApi.DetailType {
        return mType
    }

    override fun isVisible(): Boolean {
        return mIsVisible
    }

    override fun getLabel(): String {
        return mLabel
    }

    override fun getValue(): String {
        return mValue
    }

    override fun setLabel(label: String) {
        mLabel = label
    }

    override fun setValue(value: String) {
        mValue = value
    }
}
