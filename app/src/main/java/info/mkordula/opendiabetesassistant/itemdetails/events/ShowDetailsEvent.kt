package info.mkordula.opendiabetesassistant.itemdetails.events

data class ShowDetailsEvent(val logEntryId: String)