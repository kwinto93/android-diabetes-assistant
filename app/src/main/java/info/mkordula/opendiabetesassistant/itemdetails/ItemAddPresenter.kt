package info.mkordula.opendiabetesassistant.itemdetails

import info.mkordula.opendiabetesassistant.data.database.DatabaseDataProviderApi
import info.mkordula.opendiabetesassistant.navigation.MainMenuPresenter
import info.mkordula.opendiabetesassistant.utils.AsyncTaskManagerApi
import info.mkordula.opendiabetesassistant.utils.EventBusApi

class ItemAddPresenter(itemDetailsConfigApi: ItemDetailsConfigApi,
                       databaseDataProviderApi: DatabaseDataProviderApi,
                       rowsCollection: Collection<ItemDetailsRowApi>,
                       eventBusApi: EventBusApi,
                       asyncTaskManagerApi: AsyncTaskManagerApi)
    : ItemDetailsPresenter(itemDetailsConfigApi, databaseDataProviderApi, rowsCollection,
        eventBusApi, asyncTaskManagerApi) {
    override fun getType(): MainMenuPresenter.VIEW = MainMenuPresenter.VIEW.ADD
}