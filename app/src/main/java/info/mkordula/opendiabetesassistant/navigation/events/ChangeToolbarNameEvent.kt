package info.mkordula.opendiabetesassistant.navigation.events

import info.mkordula.opendiabetesassistant.navigation.MainMenuPresenter

data class ChangeToolbarNameEvent(val viewType: MainMenuPresenter.VIEW)