package info.mkordula.opendiabetesassistant.navigation.events

import info.mkordula.opendiabetesassistant.navigation.MainMenuPresenter

data class ChangeViewEvent(val viewType: MainMenuPresenter.VIEW)