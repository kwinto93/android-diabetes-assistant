package info.mkordula.opendiabetesassistant.navigation

interface MainMenuViewApi {
    fun setToolBarName(viewId: MainMenuPresenter.VIEW)
    fun showLogBook()
    fun showDetails()
    fun showAdd()
    fun showSettings()
    fun showStats()
    fun showLogBookExcludingBackStack()
    fun changeAddFabVisibility(visibility: Int)
    fun changeUser()
    fun getRestoredViewId(): MainMenuPresenter.VIEW
    fun showCharts()
}