package info.mkordula.opendiabetesassistant.navigation

import android.util.Log
import info.mkordula.opendiabetesassistant.base.BasePresenter
import info.mkordula.opendiabetesassistant.itemdetails.events.AddFabVisibilityEvent
import info.mkordula.opendiabetesassistant.navigation.events.ChangeToolbarNameEvent
import info.mkordula.opendiabetesassistant.navigation.events.ChangeViewEvent
import info.mkordula.opendiabetesassistant.navigation.events.ShowMainViewEvent
import info.mkordula.opendiabetesassistant.utils.EventBusApi
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class MainMenuPresenter(eventBusApi: EventBusApi) : BasePresenter<MainMenuViewApi>(eventBusApi) {
    companion object {
        val DEFAULT_VIEW = VIEW.LOG
    }

    var currentSelectedView = VIEW.LOG
        private set

    override fun attach(view: MainMenuViewApi) {
        super.attach(view)
        if (view.getRestoredViewId() == VIEW.UNKNOWN) {
            showMainView()
        } else {
            currentSelectedView = view.getRestoredViewId()
        }
    }

    @Suppress("UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun showMainView(event: ShowMainViewEvent) {
        showMainView()
    }

    private fun showMainView() {
        changeView(VIEW.LOG)
    }

    override fun useEventBuss() = true

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onChangeView(event: ChangeViewEvent) {
        changeView(event.viewType)
    }

    fun changeView(newViewId: VIEW) {
        currentSelectedView = newViewId
        when (newViewId) {
            VIEW.ADD -> view?.showAdd()
            VIEW.LOG -> view?.showLogBook()
            VIEW.DETAILS -> view?.showDetails()
            VIEW.SETTINGS -> view?.showSettings()
            VIEW.STATS -> view?.showStats()
            VIEW.CHANGE_USER -> view?.changeUser()
            VIEW.CHARTS -> view?.showCharts()
            else -> Log.w("changeView", "Unknown id $newViewId")
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onChangeToolbarName(event: ChangeToolbarNameEvent) {
        changeToolbarName(event.viewType)
    }

    private fun changeToolbarName(viewId: VIEW) {
        view?.setToolBarName(viewId)
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onChangeAddFabVisibility(event: AddFabVisibilityEvent) {
        view?.changeAddFabVisibility(event.visibility)
    }

    enum class VIEW(val fragmentTag: String) {
        ADD("details_content"), CHANGE_USER("change_user"), LOG("log"), STATS("stats"), SETTINGS("settings"),
        CHARTS("charts"), DETAILS("details_info"), UNKNOWN("unknown")
    }
}