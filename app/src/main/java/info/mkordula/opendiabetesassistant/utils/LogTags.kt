package info.mkordula.opendiabetesassistant.utils

enum class LogTags(val tag: String) {
    DATABASE("DATABASE"), DETAILS("DETAILS")
}