package info.mkordula.opendiabetesassistant.utils

interface EventBusApi {
    fun postSticky(event: Any)
    fun removeStickyEvent(event: Any)
    fun post(event: Any)
    fun register(subscriber: Any)
    fun unregister(subscriber: Any)
}