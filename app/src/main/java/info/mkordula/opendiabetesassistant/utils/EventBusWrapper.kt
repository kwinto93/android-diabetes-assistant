package info.mkordula.opendiabetesassistant.utils

import org.greenrobot.eventbus.EventBus

class EventBusWrapper : EventBusApi {
    override fun register(subscriber: Any) {
        EventBus.getDefault().register(subscriber)
    }

    override fun unregister(subscriber: Any) {
        EventBus.getDefault().unregister(subscriber)
    }

    override fun removeStickyEvent(event: Any) {
        EventBus.getDefault().removeStickyEvent(event)
    }

    override fun post(event: Any) {
        EventBus.getDefault().post(event)
    }

    override fun postSticky(event: Any) {
        EventBus.getDefault().postSticky(event)
    }
}