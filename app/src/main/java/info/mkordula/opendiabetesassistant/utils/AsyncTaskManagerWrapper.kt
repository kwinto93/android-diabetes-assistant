package info.mkordula.opendiabetesassistant.utils

import info.mkordula.core.AsyncTaskManager

class AsyncTaskManagerWrapper : AsyncTaskManagerApi {
    override fun <T> execute(task: () -> T?, pre: (() -> Unit)?, post: ((result: T?) -> Unit)?) {
        AsyncTaskManager.execute(task, pre, post)
    }
}