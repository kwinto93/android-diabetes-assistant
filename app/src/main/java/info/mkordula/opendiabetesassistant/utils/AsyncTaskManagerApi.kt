package info.mkordula.opendiabetesassistant.utils

interface AsyncTaskManagerApi {
    fun <T> execute(task: () -> T?, pre: (() -> Unit)?, post: ((result: T?) -> Unit)?)
}