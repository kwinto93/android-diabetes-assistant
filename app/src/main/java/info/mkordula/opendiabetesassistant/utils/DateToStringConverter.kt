package info.mkordula.opendiabetesassistant.utils

import java.text.DateFormat
import java.util.*

class DateToStringConverter {
    private val mDateFormat = DateFormat.getDateInstance(DateFormat.SHORT)
    private val mTimeFormat = DateFormat.getTimeInstance(DateFormat.SHORT)

    fun getDate(date: Date): String = mDateFormat.format(date)

    fun getTime(date: Date): String = mTimeFormat.format(date)

    fun getDateTime(date: Date): String = "${getDate(date)} ${getTime(date)}"
}