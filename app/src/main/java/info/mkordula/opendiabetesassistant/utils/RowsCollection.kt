package info.mkordula.opendiabetesassistant.utils

import android.content.Context
import info.mkordula.opendiabetesassistant.R
import info.mkordula.opendiabetesassistant.itemdetails.ItemDetailsRow
import info.mkordula.opendiabetesassistant.itemdetails.ItemDetailsRowApi

class RowsCollection(context: Context) {
    companion object {
        const val EMPTY_TEXT = ""
    }

    private val mRows = setOf(
            ItemDetailsRow(ItemDetailsRowApi.DetailType.DATE_TIME, context.getString(R.string.detail_date_time), EMPTY_TEXT),
            ItemDetailsRow(ItemDetailsRowApi.DetailType.GLUCOSE, context.getString(R.string.detail_glucose), EMPTY_TEXT),
            ItemDetailsRow(ItemDetailsRowApi.DetailType.PRIMARY_INSULIN, context.getString(R.string.detail_insulin_dose), EMPTY_TEXT),
            ItemDetailsRow(ItemDetailsRowApi.DetailType.SECONDARY_INSULIN, context.getString(R.string.detail_insulin_dose), EMPTY_TEXT),
            ItemDetailsRow(ItemDetailsRowApi.DetailType.CARBOHYDRATES_DOSE, context.getString(R.string.detail_carbohydrates), EMPTY_TEXT),
            ItemDetailsRow(ItemDetailsRowApi.DetailType.WEIGHT, context.getString(R.string.detail_weight), EMPTY_TEXT)
    )

    fun getRows(): Collection<ItemDetailsRowApi> {
        return mRows
    }
}