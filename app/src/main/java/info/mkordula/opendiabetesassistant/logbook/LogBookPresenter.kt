package info.mkordula.opendiabetesassistant.logbook

import android.view.View
import info.mkordula.core.AsyncTaskManager
import info.mkordula.opendiabetesassistant.base.BasePresenter
import info.mkordula.opendiabetesassistant.data.database.DatabaseDataProviderApi
import info.mkordula.opendiabetesassistant.data.database.events.DatabaseCacheChangedEvent
import info.mkordula.opendiabetesassistant.itemdetails.events.AddFabVisibilityEvent
import info.mkordula.opendiabetesassistant.itemdetails.events.ShowDetailsEvent
import info.mkordula.opendiabetesassistant.navigation.MainMenuPresenter
import info.mkordula.opendiabetesassistant.navigation.events.ChangeToolbarNameEvent
import info.mkordula.opendiabetesassistant.navigation.events.ChangeViewEvent
import info.mkordula.opendiabetesassistant.utils.AsyncTaskManagerApi
import info.mkordula.opendiabetesassistant.utils.DateToStringConverter
import info.mkordula.opendiabetesassistant.utils.EventBusApi
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class LogBookPresenter(private val mDatabaseDataProvider: DatabaseDataProviderApi,
                       mEventBusApi: EventBusApi,
                       private val mAsyncTaskManagerApi: AsyncTaskManagerApi)
    : BasePresenter<LogBookViewApi>(mEventBusApi) {
    private lateinit var mItems: Collection<LogListItemApi>

    override fun useEventBuss() = true

    override fun attach(view: LogBookViewApi) {
        super.attach(view)

        mEventBusApi.postSticky(ChangeToolbarNameEvent(MainMenuPresenter.VIEW.LOG))
        mEventBusApi.postSticky(AddFabVisibilityEvent(View.VISIBLE))

        startLoading()
    }

    fun deleteEntry(itemId: String) {
        mDatabaseDataProvider.deleteLogEntrySync(itemId)
    }

    fun showDetails(itemId: String) {
        mEventBusApi.post(ChangeViewEvent(MainMenuPresenter.VIEW.DETAILS))
        mEventBusApi.postSticky(ShowDetailsEvent(itemId))
    }

    fun getLowGlucoseValue(): Int = 80

    fun getHighGlucoseValue(): Int = 120

    @Suppress("UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDatabaseChanged(event: DatabaseCacheChangedEvent) {
        startLoading()
    }

    private fun loadingFinished() {
        view?.setItems(mItems)
        if (mItems.isNotEmpty()) {
            view?.hideLoadingBar()
        } else {
            view?.switchViewsWithDelay()
        }
        view?.showList()
    }

    private fun startLoading() {
        view?.showLoadingBar()
        view?.hideList()
        mAsyncTaskManagerApi.execute({
            val logEntries = mDatabaseDataProvider.getLogEntries()
            val dateToStringConverter = DateToStringConverter()
            logEntries.map {
                LogListItem(it, dateToStringConverter)
            }
        }, null, {
            mItems = it!!
            loadingFinished()
        })
    }
}