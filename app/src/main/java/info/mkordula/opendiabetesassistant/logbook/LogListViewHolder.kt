package info.mkordula.opendiabetesassistant.logbook

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import info.mkordula.opendiabetesassistant.R
import info.mkordula.opendiabetesassistant.data.models.LogEntryModel.Companion.UNKNOWN_ITEM_ID

class LogListViewHolder(mView: View,
                        private val mLogBookPresenter: LogBookPresenter)
    : RecyclerView.ViewHolder(mView) {

    @BindView(R.id.logbook_item_time)
    lateinit var mTime: TextView

    @BindView(R.id.logbook_item_date)
    lateinit var mDate: TextView

    @BindView(R.id.logbook_item_glucose)
    lateinit var mGlucose: TextView

    @BindView(R.id.logbook_item_details)
    lateinit var mExtraDetails: TextView

    @BindView(R.id.logbook_item_status)
    lateinit var mTrendIcon: ImageView

    private var mLogListItem: LogListItemApi? = null

    init {
        ButterKnife.bind(this, mView)
        mView.setOnClickListener {
            mLogBookPresenter.showDetails(getLogEntryId())
        }
        mView.setOnLongClickListener {
            mLogBookPresenter.deleteEntry(getLogEntryId())
            true
        }
    }

    private fun getLogEntryId() = mLogListItem?.getModelId() ?: UNKNOWN_ITEM_ID

    fun setLogItem(logItem: LogListItemApi) {
        mLogListItem = logItem

        mTime.text = mLogListItem?.getTimeString()
        mDate.text = mLogListItem?.getDateString()
        mGlucose.text = mLogListItem?.getGlucoseReading()
        mExtraDetails.text = mLogListItem?.getExtraDetails()

        mTrendIcon.setImageResource(
                when {
                    logItem.getGlucoseValue() == 0 -> R.drawable.ic_item_standard_info
                    logItem.getGlucoseValue() > mLogBookPresenter.getHighGlucoseValue() -> R.drawable.ic_glucose_high
                    logItem.getGlucoseValue() < mLogBookPresenter.getLowGlucoseValue() -> R.drawable.ic_glucose_low
                    else -> R.drawable.ic_glucose_good
                }
        )
    }
}