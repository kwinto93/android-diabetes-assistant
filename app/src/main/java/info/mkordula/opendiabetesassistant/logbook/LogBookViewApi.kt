package info.mkordula.opendiabetesassistant.logbook

interface LogBookViewApi {
    fun setItems(items: Collection<LogListItemApi>)
    fun showLoadingBar()
    fun hideLoadingBar()
    fun hideList()
    fun showList()
    fun switchViewsWithDelay()
}