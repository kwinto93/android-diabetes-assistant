package info.mkordula.opendiabetesassistant.logbook

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import butterknife.BindView
import info.mkordula.core.BaseFragment
import info.mkordula.core.BaseViewApi
import info.mkordula.opendiabetesassistant.R

class LogBookFragment : BaseViewApi<LogBookPresenter>, BaseFragment() {
    companion object {
        private const val SWITCH_VIEW_DELAY = 2000L
    }

    private val mViewApi = object : LogBookViewApi {
        override fun switchViewsWithDelay() {
            mProgressView.postDelayed({
                hideLoadingBar()
                showList()
            }, SWITCH_VIEW_DELAY)
        }

        override fun showList() {
            mListView.visibility = View.VISIBLE
        }

        override fun hideList() {
            mListView.visibility = View.GONE
        }

        override fun setItems(items: Collection<LogListItemApi>) {
            mItems.clear()
            mItems.addAll(items)
            mLogListAdapter?.notifyDataSetChanged()
        }

        override fun showLoadingBar() {
            mProgressView.visibility = View.VISIBLE
        }

        override fun hideLoadingBar() {
            mProgressView.visibility = View.GONE
        }
    }

    private val mItems: MutableList<LogListItemApi> = mutableListOf()

    @BindView(R.id.logbook_items_list_progress)
    lateinit var mProgressView: ProgressBar

    @BindView(R.id.logbook_items_list)
    lateinit var mListView: RecyclerView

    private lateinit var mLogBookPresenter: LogBookPresenter

    private var mLogListAdapter: LogListAdapter? = null

    override fun setPresenter(presenter: LogBookPresenter) {
        mLogBookPresenter = presenter
    }

    override fun getViewLayout(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater?.inflate(R.layout.logbook_list, container, false)!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mListView.setHasFixedSize(true)
        mListView.layoutManager = LinearLayoutManager(activity)

        mLogListAdapter = LogListAdapter(mItems, mLogBookPresenter)
        mListView.adapter = mLogListAdapter
    }

    override fun onStart() {
        super.onStart()
        mLogBookPresenter.attach(mViewApi)
    }

    override fun onStop() {
        mLogBookPresenter.detach()
        super.onStop()
    }
}