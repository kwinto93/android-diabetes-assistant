package info.mkordula.opendiabetesassistant.logbook

import info.mkordula.opendiabetesassistant.data.models.LogEntryModel
import info.mkordula.opendiabetesassistant.utils.DateToStringConverter
import java.util.*

class LogListItem(private val mLogEntryModel: LogEntryModel,
                  private val mDateToStringConverter: DateToStringConverter)
    : LogListItemApi {
    override fun getGlucoseValue(): Int {
        return mLogEntryModel.glucose
    }

    override fun getModelId(): String {
        return mLogEntryModel.key ?: LogEntryModel.UNKNOWN_ITEM_ID
    }

    companion object {
        const val EMPTY_TEXT = "n/a"
    }

    override fun getDateString(): String {
        return mDateToStringConverter.getDate(Date(mLogEntryModel.timeStamp))
    }

    override fun getTimeString(): String {
        return mDateToStringConverter.getTime(Date(mLogEntryModel.timeStamp))
    }

    override fun getGlucoseReading(): String {
        return if (mLogEntryModel.glucose > 0) mLogEntryModel.glucose.toString() else EMPTY_TEXT
    }

    override fun getExtraDetails(): String {
        return if (mLogEntryModel.carbohydratesDose > 0
                || mLogEntryModel.primaryInsulinDose > 0
                || mLogEntryModel.secondaryInsulinDose > 0
                || mLogEntryModel.weight > 0)
            "${mLogEntryModel.carbohydratesDose} WW, ${mLogEntryModel.primaryInsulinDose} u, ${mLogEntryModel.secondaryInsulinDose} u, ${mLogEntryModel.weight} kg"
        else
            EMPTY_TEXT
    }
}