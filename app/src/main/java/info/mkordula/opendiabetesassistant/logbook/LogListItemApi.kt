package info.mkordula.opendiabetesassistant.logbook

interface LogListItemApi {
    fun getDateString(): String
    fun getTimeString(): String
    fun getGlucoseReading(): String
    fun getExtraDetails(): String
    fun getModelId(): String
    fun getGlucoseValue(): Int
}