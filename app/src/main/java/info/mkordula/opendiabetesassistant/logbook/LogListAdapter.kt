package info.mkordula.opendiabetesassistant.logbook

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import info.mkordula.opendiabetesassistant.R

class LogListAdapter(private val mEntriesList: Collection<LogListItemApi>,
                     private val mLogBookPresenter: LogBookPresenter)
    : RecyclerView.Adapter<LogListViewHolder>() {
    override fun getItemCount(): Int {
        return mEntriesList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.logbook_list_item, parent, false)
        return LogListViewHolder(view, mLogBookPresenter)
    }

    override fun onBindViewHolder(holder: LogListViewHolder, position: Int) {
        val entry = mEntriesList.elementAt(position)
        holder.setLogItem(entry)
    }
}