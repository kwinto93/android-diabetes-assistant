package info.mkordula.opendiabetesassistant.authentication

import android.app.Activity
import com.google.firebase.auth.FirebaseUser

interface AuthenticationManagerApi {
    fun isAuthenticated(): Boolean
    fun getUser(): FirebaseUser?
    fun triggerAuthUi(activity: Activity, authUiRequestId: Int)
}