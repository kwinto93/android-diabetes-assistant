package info.mkordula.opendiabetesassistant.authentication

import android.app.Activity
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class AuthenticationManager : AuthenticationManagerApi {
    override fun triggerAuthUi(activity: Activity, authUiRequestId: Int) {
        activity.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .build(), authUiRequestId)
    }

    private val mFirebaseAuth = FirebaseAuth.getInstance()

    override fun isAuthenticated(): Boolean = mFirebaseAuth.currentUser != null

    override fun getUser(): FirebaseUser? = mFirebaseAuth.currentUser
}